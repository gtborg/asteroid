/**
 * Utility functions for Asteroid project
 * @author Andrew Melim
 */
#pragma once
#include <string>
#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include "LBLData.h"
#include <monoslam/BatchMono.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/geometry/SimpleCamera.h>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/export.hpp>

typedef gtsam::GenericProjectionFactor<gtsam::Pose3, gtsam::Point3, gtsam::Cal3_S2> GPF;
typedef gtsam::PriorFactor<gtsam::Pose3> PPF;
typedef gtsam::NoiseModelFactor1<gtsam::Pose3> NMF;

/**
 * Loads LatLongAlt information from FITS files
 * http://sbn.psi.edu/archive/dawn/fc/DWNVFC2_1A/DOCUMENT/SIS/DAWN_FC_SIS_20130313.PDF
 */
std::vector<ast::LBLData> loadLLAOrbit(const std::string &orbitDir);

/**
 * Initializes camera positions from latlongalt measurements. Backprojects points
 * @param llpVec     LatLongAlt vector
 * @param hypergraph HyperGraph with truss initialized
 */
gtsam::Values initLLAOrbit(std::vector<ast::LBLData> llpVec,
  const monoslam::DSFEdgeMap& edgeMap, std::set<int> cams, gtsam::Cal3_S2::shared_ptr K);

/**
 * Checks for intersection of two DAWN camera images
 * @param  cam1 LBL data for first camera
 * @param  cam2 LBL data for second camera
 * @return      True if intersect, otherwise false
 */
bool intersect(const ast::LBLData& cam1, const ast::LBLData& cam2);

/**
 * Returns the total time between two poses in seconds. Assumes l1 < l2
 * @param  l1 LBL data for first camera
 * @param  l2 LBL data for second camera
 * @return    Time in seconds (up to millisecond precision)
 */
double totalTimeBetween(const ast::LBLData& l1, const ast::LBLData& l2);

/*******************************************************************************
                                 Serializaiton
*******************************************************************************/

BOOST_CLASS_EXPORT(gtsam::Symbol);
BOOST_CLASS_EXPORT(gtsam::Point3);
BOOST_CLASS_EXPORT(gtsam::Pose3);
BOOST_CLASS_EXPORT(gtsam::Rot3);
BOOST_CLASS_EXPORT(gtsam::Cal3_S2);
BOOST_CLASS_EXPORT(gtsam::NonlinearFactor);
BOOST_CLASS_EXPORT(gtsam::NoiseModelFactor);
BOOST_CLASS_EXPORT(gtsam::NonlinearFactorGraph);
BOOST_CLASS_EXPORT(PPF);
BOOST_CLASS_EXPORT(GPF);
BOOST_CLASS_EXPORT(NMF);
BOOST_CLASS_EXPORT(gtsam::noiseModel::Base);
BOOST_CLASS_EXPORT(gtsam::noiseModel::Isotropic);
BOOST_CLASS_EXPORT(gtsam::noiseModel::Gaussian);
BOOST_CLASS_EXPORT(gtsam::noiseModel::Diagonal);
BOOST_CLASS_EXPORT(gtsam::noiseModel::Unit);

/*******************************************************************************/
template<class T>
bool writeGTSAM(std::string& filename, boost::shared_ptr<T> obj){
  std::cout << "Writing: " << filename << std::endl;
  try{
    std::ofstream writerStream(filename.c_str(), std::ios::binary);
    boost::archive::binary_oarchive writer(writerStream);
    writer << obj;
    writerStream.close();
    return true;
  }catch(std::exception& e){
    std::cout << e.what() << std::endl;
    return false;
  }
}
/*******************************************************************************/
template<class T>
boost::shared_ptr<T> loadGTSAM(std::string& filename){
  boost::shared_ptr<T> obj;
  std::cout << "Reading input file " << filename << std::endl;
  try{
  std::ifstream readerStream(filename.c_str(), std::ios::binary);
  boost::archive::binary_iarchive reader(readerStream);
  reader >> obj;
  return obj;
  }catch(std::exception& e){
    std::cout << e.what() << std::endl;
    // Return uninitialized ptr
    return boost::shared_ptr<T>();
  }
}