/*
 * @file LBLData.h
 * @author Andrew Melim
 */

#pragma once

//GTSAM
#include <gtsam/geometry/Point3.h>

//BOOST
#include "boost/date_time/posix_time/posix_time.hpp"

#include <math.h>
#include <iostream>
#define _USE_MATH_DEFINES

namespace ast {

class LBLData
{
  private:
    std::string filename_;
    // Sub-spacecraft planetocentric latitude (N/A for calibration targets)
    double subScLat_; // rad
    // Sub-spacecraft planetocentric east longitude (N/A for calibration targets)
    double subScLon_; // rad
    // S/C altitude relative to the target
    double scAlt_; // km
    // North azimuth angle evaluated at the center pixel of the image/spectra
    double na_; // rad
    // Instrument distance to target center (center of asteroid body)
    double dist_; // km
    // Following four values describe four corners of the image plane
    double minLat_; // This specifies the southernmost latitude of a spatial area
    double maxLat_; // This specifies the northernmost latitude of a spatial area
    double westLon_;
    double eastLon_;
    // Time info
    boost::posix_time::ptime endTime_;
    // Velocities
    double vx_;
    double vy_;
    double vz_;

    double x_;
    double y_;
    double z_;

  public:

    LBLData() : filename_(""), subScLat_(0), subScLon_(0), scAlt_(0), na_(0),
     dist_(0), minLat_(0), maxLat_(0), westLon_(0), eastLon_(0),
     vx_(0), vy_(0), vz_(0), x_(0), y_(0), z_(0)  {}

    /*Get Methods*/
    std::string filename() const{
      return filename_;
    }
    double latitude() const {
     return subScLat_;
    }

    double longitude() const {
     return subScLon_;
    }

    double altitude() const {
     return scAlt_;
    }

    double northAzimuth() const {
     return na_;
    }

    double minLatitude() const {
     return minLat_;
    }

    double maxLatitude() const {
     return maxLat_;
    }

    double westLongitude() const {
     return westLon_;
    }

    double eastLongitude() const {
     return eastLon_;
    }

    boost::posix_time::ptime endTime() const {
      return endTime_;
    }

    // Returns velocity vector in km/s
    gtsam::Vector3 velocity() const{
      return (gtsam::Vector(3) << vx_, vy_, vz_);
    }

    // Returns position in kilometers
    gtsam::Point3 position() const{
      return gtsam::Point3(x_,y_,z_);
    }

    gtsam::Point3 position_m() const{
      // Converts into meters
      double x = dist_*1000. * cos(subScLat_) * cos(subScLon_);
      double y = dist_*1000. * cos(subScLat_) * sin(subScLon_);
      double z = dist_*1000. * sin(subScLat_);
      return gtsam::Point3(x,y,z);
    }

    /*Set Methods*/
    void setFilename(std::string& filename) { filename_ = filename; }

    void setMinLatitude(double minLat) { minLat_ = minLat; }

    void setMaxLatitude(double maxLat) { maxLat_ = maxLat; }

    void setWestLongitude(double westLon) { westLon_ = westLon; }

    void setEastLongitude(double eastLon) { eastLon_ = eastLon; }

    void setLatitude(double lat) { subScLat_ = lat; }

    void setLongitude(double lon) { subScLon_ = lon; }

    void setAltitude(double alt) { scAlt_ = alt; }

    void setNorthAzimuth(double na){ na_ = na;}

    void setTargetDistance(double dist){ dist_ = dist;}

    void setVX(double vx) { vx_ = vx; }
    void setVY(double vy) { vy_ = vy; }
    void setVZ(double vz) { vz_ = vz; }

    void setX(double x) { x_ = x; }
    void setY(double y) { y_ = y; }
    void setZ(double z) { z_ = z; }

    void setEndTime(boost::posix_time::ptime& endTime){
      endTime_ = endTime;
    }

    void setLLA(double lat, double lon, double alt) {
      subScLat_ = lat;
      subScLon_ = lon;
      scAlt_ = alt;
    }
};

}
