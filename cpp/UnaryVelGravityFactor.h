/**
 * @file UnaryVelGravityFactor
 * @brief Factor for estimating spherical harmonics of a gravitational field and velocity, assumes position measurements
 * @author Andrew Melim
 */

#pragma once

//BOOST
#include <boost/lexical_cast.hpp>
#include <boost/math/special_functions/legendre.hpp>
#include <boost/math/special_functions/factorials.hpp>
//GTSAM
#include <gtsam/geometry/concepts.h>
#include <gtsam/base/Vector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/geometry/Point3.h>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/nonlinear/NonlinearFactor.h>
//AST
#include "LBLData.h"

#include <math.h>
#include <fstream>

#define _USE_MATH_DEFINES

namespace ast {

  class UnaryVelGravityFactor : public gtsam::NoiseModelFactor2<gtsam::LieVector, gtsam::LieVector>
  {

    private:

      gtsam::Point3 pose1_;
      gtsam::Point3 pose2_;

      //Degree of our coeffs
      size_t dim_;
      size_t nCof_;

      //Total time between two poses
      double total_;

      //Integration step size
      double dt_;

      // Constants
      double mu; // km^3 / s^2
      double Re; // km

    public:
      typedef gtsam::NoiseModelFactor2<gtsam::LieVector, gtsam::LieVector> Base;
      typedef UnaryVelGravityFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

      //Default Constructor
      UnaryVelGravityFactor() : mu(17.28825), Re(265.0){}

      UnaryVelGravityFactor(gtsam::Point3& init, gtsam::Point3& final,
        gtsam::SharedNoiseModel& model, gtsam::Key cofKey,  gtsam::Key velKey, size_t dim, double total, double dt);


      virtual ~UnaryVelGravityFactor() {}

      /* ************************************************************************* */
      //Make deep copy
      gtsam::NonlinearFactor::shared_ptr clone() const{
        return boost::static_pointer_cast<gtsam::NonlinearFactor>(
            gtsam::NonlinearFactor::shared_ptr(new UnaryVelGravityFactor(*this)));
      }

      /* ************************************************************************* */
      void print(const std::string& s, const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /* ************************************************************************* */
      size_t numCoefs() const{
        return nCof_;
      }

      /* ************************************************************************* */
      bool equals(const gtsam::NonlinearFactor& p, double tol=1e-9) const {
        const This* e = dynamic_cast<const This*>(&p);
        return e != NULL
          && Base::equals(p, tol)
          && (dt_ - e->dt_) < tol;
          //&& this->vel_.equals(e->vel_,tol);
      }

      /* ************************************************************************* */
      gtsam::Vector evaluateError(const gtsam::LieVector& cof, const gtsam::LieVector& vel,
        boost::optional<gtsam::Matrix&> H1 = boost::none, boost::optional<gtsam::Matrix&> H2 = boost::none) const{

        if (H1) {
          *H1 = gtsam::numericalDerivative11<gtsam::LieVector, gtsam::LieVector>(boost::bind(&This::predict, this, _1, vel), cof);
        }
        if (H2) {
          *H2 = gtsam::numericalDerivative11<gtsam::LieVector, gtsam::LieVector>(boost::bind(&This::predict, this, cof, _1), vel);
        }


        gtsam::Vector error(predict(cof, vel));

        return error;
      }

      /* ************************************************************************* */
      // Returns the error of the predicted position given the coefficients
      gtsam::LieVector predict(const gtsam::LieVector& cof, const gtsam::LieVector& vel) const
      {
        double t = 0;
        gtsam::Point3 pose(pose1_);
        gtsam::Vector3 v(vel);
        gtsam::Vector3 a;

        while(t < total_){
          pose = pose.compose(gtsam::Point3(v*dt_));

          gtsam::Vector3 a_p = gravityAcceleration(cof, pose); // Perturbations

          // Cowell's Formula Vollado14 pg553
          a = -(mu*pose.vector()/pow(pose.norm(),3)) + a_p;


          v = v + a*dt_;

          t += dt_;
        }


        gtsam::Point3 delta = pose2_.between(pose);
        return gtsam::Point3::Logmap(delta);
      }

      /* ************************************************************************* */
      //See mathematica notebook in docs for more information
      //Uses matlab's normalization computation
      double normedLegendre(const int n, const int m, const double x) const{

        // Factorial is undefined for negative numbers
        if(n-m < 0)
          return 0.0;

        double fact_num = boost::math::factorial<double>(n - m);
        double fact_denom = boost::math::factorial<double>(n + m);

        return boost::math::legendre_p(n,m,x) * pow(-1.0,m)*sqrt((((double)n + (1.0/2.0))*fact_num)/fact_denom);
      }

      /* ************************************************************************* */
      // Compute the acceleration vector provided
      gtsam::Vector3 gravityAcceleration(const gtsam::LieVector& coefficients, const gtsam::Point3& pos) const {

        //int s_dim = (-3+sqrt(9+8*dim))/2 + 1;
        // Reference Radius Konopliv 12
        // Gravitational constant
        // Derivatives
        double dudr = 0;
        double dudlon = 0;
        double dudlat = 0;

        // radius
        double r = pos.norm();
        // sin(lat)
        double sLat = pos.z() / r;
        // tan(lat)
        double tLat = pos.z() / sqrt(pow(pos.x(),2) + pow(pos.y(),2));
        double lon = atan2(pos.y(), pos.x());

        size_t s_cof_Index=0;
        size_t c_cof_Index=2;

        // HARDCODED
        if(dim_ == 3){
          s_cof_Index=0;
          c_cof_Index=5;
        }


        //N 1 -> inf
        for(size_t n = 2; n < dim_+1; n++)// fix this manual hack
        {
          for(size_t m = 0; m <= n; m++)
          {
              double c_cof = coefficients[c_cof_Index];
              double s_cof = 0.0;
              c_cof_Index++;
              if(m > 0){
                s_cof = coefficients[s_cof_Index];
                s_cof_Index++;
              }

              double delta_dudr =  (mu / pow(r,2)) * pow((Re / r),n) * (n + 1) //
                  * normedLegendre(n,m,sLat) * (c_cof * cos(m * lon) + s_cof * sin(m * lon));

              double delta_dudlat =  (mu / r) * pow((Re / r),n)
                * (normedLegendre(n,m+1,sLat) - m*tLat*normedLegendre(n,m,sLat))
                * (c_cof * cos(m * lon) + s_cof * sin(m * lon));

              double delta_dudlon =  (mu / r) * pow((Re / r),n) * m * normedLegendre(n,m,sLat)
                * (s_cof * cos(m * lon) - c_cof * sin(m * lon));

              // Derivative of the potential wrt r
              dudr = dudr - delta_dudr;

              // Derivative of the potential wrt lat
              dudlat = dudlat - delta_dudlat;

              // Derivative of the potential wrt lon
              dudlon = dudlon +  delta_dudlon;

          }
        }

        // Cartesian accelerations
        double ax = ((1/r)*dudr - pos.z() / (pow(r,2) * sqrt(pow(pos.x(),2) + pow(pos.y(),2)))*dudlat) * pos.x()
           - ((1/(pow(pos.x(),2) + pow(pos.y(),2))) * dudlon) * pos.y();

        double ay = ((1/r)*dudr - pos.z() / (pow(r,2) * sqrt(pow(pos.x(),2) + pow(pos.y(),2)))*dudlat) * pos.y()
           + ((1/(pow(pos.x(),2) + pow(pos.y(),2))) * dudlon) * pos.x();

        double az = (1/r) * dudr *pos.z() + (sqrt(pow(pos.x(),2) + pow(pos.y(),2))/pow(r,2))*dudlat;

       gtsam::Vector accel = (gtsam::Vector(3) << ax, ay, az);
       return accel;
      }

  };//End Class
} //End Namespace
