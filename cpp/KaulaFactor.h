/**
 * @file KaulaFactor
 * @brief Kaula Powerlaw Constraint see Konopliv11ssr
 * @author Andrew Melim
 */

//GTSAM
#include <gtsam/geometry/concepts.h>
#include <gtsam/base/Vector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/geometry/Point3.h>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/nonlinear/NonlinearFactor.h>

namespace ast {

  class KaulaFactor : public gtsam::NoiseModelFactor1<gtsam::LieVector>
  {
    private:
      int dim_; // Degree of the coefficients

    public:
      typedef gtsam::NoiseModelFactor1<gtsam::LieVector> Base;
      typedef KaulaFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

      KaulaFactor(gtsam::Key cofKey, gtsam::SharedNoiseModel& model, int d) : Base(model, cofKey), dim_(d) {}

      virtual ~KaulaFactor(){}

      /* ************************************************************************* */
      //Make deep copy
      gtsam::NonlinearFactor::shared_ptr clone() const{
        return boost::static_pointer_cast<gtsam::NonlinearFactor>(
            gtsam::NonlinearFactor::shared_ptr(new KaulaFactor(*this)));
      }

      /* ************************************************************************* */

      /**
       * Returns error from kaula power law Konopliv11ssr Eq. 4
       * @param  coefficients Harmonic coefficients for gravitational potential
       * @param  H1  Jacobian
       * @return     Error
       */
      gtsam::Vector evaluateError(const gtsam::LieVector& coefficients,
        boost::optional<gtsam::Matrix&> H1 = boost::none) const{

        if (H1) {
          *H1 = gtsam::numericalDerivative11<gtsam::LieVector>(boost::bind(&This::predict, this, _1), coefficients);
        }

        gtsam::Vector error(dim_-1);
        gtsam::Vector expected(dim_-1);


        double K = 0.011; // Vesta constant

        for(int n=2; n < dim_+1; n++){

          double expect = K/pow(n,2);
          expected(n-2) = expect;
        }

        gtsam::Vector predicted = predict(coefficients);
        error = expected - predicted;

        return error;
      }


      gtsam::Vector predict(const gtsam::LieVector& coefficients) const{
        gtsam::Vector predicted(dim_-1);

        size_t s_cof_Index=0;
        size_t c_cof_Index=2;

        // HARDCODED
        if(dim_ == 3){
          s_cof_Index=0;
          c_cof_Index=5;
        }

        for(int n=2; n < dim_+1; n++){
          double sum = 0;

          for(int m = 0; m < n; m++){

            double c_cof = coefficients[c_cof_Index];
            double s_cof = 0.0;
            c_cof_Index++;
            if(m > 0){
              s_cof = coefficients[s_cof_Index];
              s_cof_Index++;
            }

            sum += pow(c_cof,2) + pow(s_cof,2);
          }

          double Mn = sqrt(sum/(2*n + 1));

          predicted(n-2) = Mn;
        }

        return predicted;
      }
  };
}