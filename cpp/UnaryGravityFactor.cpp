#include "UnaryGravityFactor.h"


using namespace gtsam;

namespace ast {

  UnaryGravityFactor::UnaryGravityFactor(gtsam::Vector3& vel, gtsam::Point3& init, gtsam::Point3& final,
    gtsam::SharedNoiseModel& model, gtsam::Key cofKey, size_t dim, double total, double dt)
    : Base(model, cofKey), vel_(vel), pose1_(init), pose2_(final), dim_(dim), total_(total), dt_(dt), mu(17.28825), Re(265.0){

    if(dim < 2){
      printf("Dimension must be greater than 1!\n");
      throw;
    }

    nCof_ = 0;
    /**
     * nCof_ counts the number of coffecients for both c and s
     * Coefficient indecies are combination of n=1->n_ m=0->n
     * nCof_ for dim 2 = 5 (20 21 22)
     * nCof_ for dim 3 = 9 (20 21 22 30 31 32 33)
     */
    for(size_t i = 2; i <= dim; i++){
      nCof_ += i+1;
    }
  }

  void UnaryGravityFactor::print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter) const {
    std::cout << s << "Gravity factor" << std::endl;
  }
}
