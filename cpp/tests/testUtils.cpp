/**
 * @author Andrew Melim
 */

#include <CppUnitLite/TestHarness.h>
#include "utils.h"
#include "LBLData.h"

using namespace std;
using namespace ast;
using namespace gtsam;

TEST(utils, loadOrbit){
  string dir = "../../../../data/DAWN/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01";
  vector<LBLData> llpVec = loadLLAOrbit(dir);
  cout << llpVec.size() << endl;
  EXPECT(llpVec.size() == 272);
  EXPECT(llpVec[0].westLongitude() == (208.8045546293 * M_PI / 180));
  Vector velExpected((Vector(3) << 71.7528, -115.346, -15.3716));
  Vector actual(llpVec[0].velocity());

  //EXPECT(velExpected == llpVec[0].velocity());
  // Fails?
  //EXPECT(assert_equal(velExpected, actual));

  Point3 expectedPos(-87.066, 54.053, -928.690);
  EXPECT(assert_equal(expectedPos, llpVec[0].position()));

  //Point3 pos(llpVec[0].position());
  //Point3 expectedPos(537130, -769991, -197721);
  //EXPECT(assert_equal(expectedPos, pos));
  //LBLData converted(pos);
  //EXPECT_DOUBLES_EQUAL(converted.latitude(), llpVec[0].latitude(), 1e-5);

  EXPECT(intersect(llpVec[0], llpVec[1]));
  EXPECT(intersect(llpVec[0], llpVec[15]));
  EXPECT(!intersect(llpVec[0], llpVec[30]));
  EXPECT(!intersect(llpVec[0], llpVec[272]));

  //cout << llpVec[0].endTime() << endl;
  //EXPET(llpVec.endTime() )
}

/* *********************** */
int main() { TestResult tr; return TestRegistry::runAllTests(tr);}
/* *********************** */
