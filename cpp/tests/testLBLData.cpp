/*
 * @file testLatLongPosition
 * @author Andrew Melim
 */

#include <CppUnitLite/TestHarness.h>
#include "LBLData.h"
#include <gtsam/geometry/Point3.h>

using namespace gtsam;
using namespace std;

TEST(LBLData, constructor)
{
  //Point3 point(0,500000, 0);
  //ast::LBLData actual(point);
//
  //EXPECT_DOUBLES_EQUAL(0, actual.latitude(), 1e-9);
  //EXPECT_DOUBLES_EQUAL(90, actual.longitude(), 1e-9);
  //EXPECT_DOUBLES_EQUAL(500000, actual.altitude(), 1e-9);
}

TEST(LBLData, setters)
{
  //ast::LBLData actual(0.0,0.0,0.0);
  //actual.setLatitude(200);
  //actual.setLongitude(100);
  //actual.setAltitude(200);
//
  //EXPECT_DOUBLES_EQUAL(200, actual.latitude(), 1e-9);
  //EXPECT_DOUBLES_EQUAL(100, actual.longitude(), 1e-9);
  //EXPECT_DOUBLES_EQUAL(200, actual.altitude(), 1e-9);
}
/* *********************** */
int main() { TestResult tr; return TestRegistry::runAllTests(tr);}
/* *********************** */
