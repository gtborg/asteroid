 /*
 * @file testUnaryGravityFactor
 * @author Andrew Melim
 */

#include <CppUnitLite/TestHarness.h>
#include "UnaryGravityFactor.h"
#include "utils.h"

#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include <boost/math/special_functions/legendre.hpp>
#include <boost/math/special_functions/factorials.hpp>
#include <boost/lexical_cast.hpp>

using namespace gtsam;
using namespace std;
using namespace ast;

static double dt_ = 0.01;
static size_t n_ = 2;
static Vector coefficients = (Vector(6) << 0.0, 0.1, 0.0, 0.0, 0.1, 0.0);// c cofs
static const double mu = 17.28825; // km^3 / s^2
static const double Re = 265.0; // km

/* ************************************************************************* */
TEST(GravityFactor, constructor){

  gtsam::Key c(1);

  Point3 initial(537130, -769991, -197721); //FC21A0010040_11285085226F1T.LBL
  Vector3 vel((Vector(3) << 16.3552, -24.1282, 130.1240)); //FC21A0010040_11285085226F1T.LBL
  Point3 expected(540745, -775335, -166458); //FC21A0010041_11285085626F1T.LBL;

  LBLData lbl;

  SharedNoiseModel model(noiseModel::Isotropic::Sigma(6, 1.0));

  ast::UnaryGravityFactor f(vel, initial, expected, model, c, n_, 0.0, dt_);
  cout << f.numCoefs() << endl;
  EXPECT(3 == f.numCoefs());
}

/* ************************************************************************* */
TEST(GravityFactor, partialDerivatives){

  // Inputs
  Point3 pos(537.130, -769.991, -197.721);
  // s21 s22 J2 c21 c22
  gtsam::LieVector coefficients((Vector(5) << -1.13e-9, 4.2469730e-3, -3.1779397e-2 , 1.23e-9, 1.0139517e-3));

  // Function
  gtsam::Vector s_cofV = gtsam::sub(coefficients, 0, 2); // 0 -> nCof-1
  //std::cout << "s_cofV \n" << s_cofV << std::endl;
  gtsam::Vector c_cofV = gtsam::sub(coefficients, 2, coefficients.size()); // nCof -> 2*

  size_t c_cof_Index=0;

  double dudr = 0;
  double dudlon = 0;
  double dudlat = 0;

  double expected_r = 959.4209;
  double expected_sLat = -0.2061;
  double expected_tLat = -0.2106;
  double expected_lon = -0.9617;

  double r = pos.norm();
  double sLat = pos.z() / r;
  double tLat = pos.z() / sqrt(pow(pos.x(),2) + pow(pos.y(),2));
  double lon = atan2(pos.y(), pos.x());

  EXPECT_DOUBLES_EQUAL(expected_r, r, 1e-3);
  EXPECT_DOUBLES_EQUAL(expected_sLat, sLat, 1e-3);
  EXPECT_DOUBLES_EQUAL(expected_tLat, tLat, 1e-5);
  EXPECT_DOUBLES_EQUAL(expected_lon, lon, 1e-5);

  int n = 2;
  int m = 0;
  double c_cof = c_cofV[c_cof_Index];

  EXPECT_DOUBLES_EQUAL(-0.0318, c_cof, 1e-3);

  double s_cof = 0.0;

  EXPECT_DOUBLES_EQUAL((mu / pow(r,2)), 1.8782e-05, 1e-5);
  EXPECT_DOUBLES_EQUAL(boost::math::legendre_p(n,m,sLat), -0.4363, 1e-3);
  EXPECT_DOUBLES_EQUAL(pow((Re / r),n), 0.0763, 1e-5);

  double expected_dudr = -5.9601e-08;
  dudr = dudr - (mu / pow(r,2)) * pow((Re / r),n) * (n + 1) //
    * boost::math::legendre_p(n,m,sLat) * (c_cof * cos(m * lon) + s_cof * sin(m * lon));
  EXPECT_DOUBLES_EQUAL(expected_dudr, dudr, 1e-5);

  double expected_dudlat = 2.6430e-05;
  dudlat = dudlat - (mu / r) * pow((Re / r),n)
    * (boost::math::legendre_p(n,m+1,sLat) - m*tLat*boost::math::legendre_p(n,m,sLat)) * (c_cof * cos(m * lon) + s_cof * sin(m * lon));
  EXPECT_DOUBLES_EQUAL(expected_dudlat, dudlat, 1e-5);

  double expected_dudlon = 0;
  // Derivative of the potential wrt lon
  dudlon = dudlon + (mu/r) * pow((Re / r),n) * m * boost::math::legendre_p(n,m,sLat)
    * (s_cof * cos(m * lon) - c_cof * sin(m * lon));
  EXPECT_DOUBLES_EQUAL(expected_dudlon, dudlon, 1e-5);
}

/* ************************************************************************* */
TEST(GravityFactor, accelerations){
  // Inputs
  Point3 pos(537.130, -769.991, -197.721);
  gtsam::LieVector coefficients((Vector(5) << -1.13e-9, 4.2469730e-3, -3.1779397e-2 , 1.23e-9, 1.0139517e-3));

  // Function

  //int s_dim = (-3+sqrt(9+8*dim))/2 + 1;
  //Reference Radius Konopliv 12
  // Gravitational constant
  // Derivatives
  double dudr = 0;
  double dudlon = 0;
  double dudlat = 0;

  // radius
  double r = pos.norm();
  // sin(lat)
  double sLat = pos.z() / r;
  // tan(lat)
  double tLat = pos.z() / sqrt(pow(pos.x(),2) + pow(pos.y(),2));
  double lon = atan2(pos.y(), pos.x());

  size_t s_cof_Index=0;
  size_t c_cof_Index=2;


  //N 1 -> inf
  for(int n = 2; n < 3; n++)// fix this manual hack
  {
    for(int m = 0; m <= n; m++)
    {
        double c_cof = coefficients[c_cof_Index];
        double s_cof = 0.0;
        c_cof_Index++;
        if(m > 0){
          s_cof = coefficients[s_cof_Index];
          s_cof_Index++;
        }
        //cout << "s cof: " << s_cof_Index << " , " << s_cof << endl;
        //cout << "c cof: " << c_cof_Index << " , " << c_cof << endl;

        double delta_dudr =  (mu / pow(r,2)) * pow((Re / r),n) * (n + 1) //
            * boost::math::legendre_p(n,m,sLat) * (c_cof * cos(m * lon) + s_cof * sin(m * lon));

        double delta_dudlat =  (mu / r) * pow((Re / r),n)
          * (boost::math::legendre_p(n,m+1,sLat) - m*tLat*boost::math::legendre_p(n,m,sLat))
          * (c_cof * cos(m * lon) + s_cof * sin(m * lon));

        double delta_dudlon =  (mu / r) * pow((Re / r),n) * m * boost::math::legendre_p(n,m,sLat)
          * (s_cof * cos(m * lon) - c_cof * sin(m * lon));

        if(m==0){
          EXPECT_DOUBLES_EQUAL(0.0, s_cof, 1e-9);
          EXPECT_DOUBLES_EQUAL(-3.1779397e-2, c_cof, 1e-9);
        }

        if(m==1){
          EXPECT_DOUBLES_EQUAL(-1.13e-9, s_cof, 1e-9);
          EXPECT_DOUBLES_EQUAL(1.23e-9, c_cof, 1e-9);
          EXPECT(s_cof_Index == 1);
          EXPECT_DOUBLES_EQUAL(4.2402e-15, delta_dudr, 1e-9);
          EXPECT_DOUBLES_EQUAL(3.0131e-13, delta_dudlon, 1e-9);
        }

        if(m==2){
          EXPECT_DOUBLES_EQUAL(4.2469730e-3, s_cof, 1e-9);
          EXPECT_DOUBLES_EQUAL(1.0139517e-3, c_cof, 1e-9);

          EXPECT_DOUBLES_EQUAL(-5.3540e-08, delta_dudr, 1e-9);
          EXPECT_DOUBLES_EQUAL(-4.0680e-06, delta_dudlon, 1e-9);
        }

        // Derivative of the potential wrt r
        dudr = dudr - delta_dudr;

        // Derivative of the potential wrt lat
        dudlat = dudlat - delta_dudlat;

        // Derivative of the potential wrt lon
        dudlon = dudlon +  delta_dudlon;

        //cout << dudr << endl;
        //cout << dudlat << endl;
        //cout << dudlon << endl;
    }
  }

  double expected_dudr = -6.0613e-09;
  EXPECT_DOUBLES_EQUAL(expected_dudr, dudr, 1e-9);
  double expected_dudlat =  3.3642e-05;
  EXPECT_DOUBLES_EQUAL(expected_dudlat, dudlat, 1e-9);
  double expected_dudlon = -4.0680e-06;
  EXPECT_DOUBLES_EQUAL(expected_dudlon, dudlon, 1e-9);

  // Cartesian accelerations
  double ax = ((1/r)*dudr - pos.z() / (pow(r,2) * sqrt(pow(pos.x(),2) + pow(pos.y(),2)))*dudlat) * pos.x()
     - ((1/(pow(pos.x(),2) + pow(pos.y(),2))) * dudlon) * pos.y();
  double expected_ax = -2.8128e-09;
  EXPECT_DOUBLES_EQUAL(expected_ax, ax, 1e-9);

  double ay = ((1/r)*dudr - pos.z() / (pow(r,2) * sqrt(pow(pos.x(),2) + pow(pos.y(),2)))*dudlat) * pos.y()
     + ((1/(pow(pos.x(),2) + pow(pos.y(),2))) * dudlon) * pos.x();
  double expected_ay = -3.5413e-09;
  EXPECT_DOUBLES_EQUAL(expected_ay, ay, 1e-9);

  double az = (1/r) * dudr *pos.z() + (sqrt(pow(pos.x(),2) + pow(pos.y(),2))/pow(r,2))*dudlat;
  double expected_az = 3.5562e-08;
  EXPECT_DOUBLES_EQUAL(expected_az, az, 1e-9);
}

TEST(GravityFactor, predict){
  gtsam::LieVector coefficients((Vector(5) << -1.13e-9, 4.2469730e-3, -3.1779397e-2 , 1.23e-9, 1.0139517e-3));


  std::vector<ast::LBLData> lbl = loadLLAOrbit("/home/borg/borg/asteroid/cpp/tests/data");
  EXPECT(lbl.size() == 2);

  boost::posix_time::time_period tp(lbl[0].endTime(), lbl[1].endTime());
  boost::posix_time::time_duration td = tp.length();
  double total = (double)td.total_milliseconds() / 1000;

  cout << "Total time: " << total << " seconds" << endl;

  gtsam::Key c(1);
  Point3 initial(537.130, -769.991, -197.721); //FC21A0010040_11285085226F1T.LBL
  Vector3 vel((Vector(3) << .0163552, -.0241282, .1301240)); //FC21A0010040_11285085226F1T.LBL
  Point3 final(540.745, -775.335, -166.458); //FC21A0010041_11285085626F1T.LBL;

  SharedNoiseModel model(noiseModel::Isotropic::Sigma(6, 1.0));

  ast::UnaryGravityFactor f(vel, initial, final, model, c, n_, total, dt_);

  Vector actual = f.predict(coefficients);
  std::cout << "Error! \n " << actual << std::endl;
  Vector expected((Vector(3) << 0, 0, 0));
  EXPECT(assert_equal(expected, actual,1e-2));


  gtsam::Vector3 a_p = f.gravityAcceleration(coefficients, initial);
  gtsam::Vector3 a = -(mu*initial.vector()/pow(initial.norm(),3)) + a_p;
  std::cout << "Accel! \n " << a.norm() << std::endl;


}

TEST(GravityFactor, predict3){
  gtsam::LieVector coefficients((Vector(12) << -1.13e-9, 4.2469730e-3,
                                                1.6820966e-3, -1.2177599e-3, 1.5466248e-4,
                                                -3.1779397e-2 , 1.23e-9, 1.0139517e-3,
                                                -3.3105530e-3, 2.0456938e-3, 6.5144047e-4, 2.3849359e-3));


  std::vector<ast::LBLData> lbl = loadLLAOrbit("/home/borg/borg/asteroid/cpp/tests/data");
  EXPECT(lbl.size() == 2);

  boost::posix_time::time_period tp(lbl[0].endTime(), lbl[1].endTime());
  boost::posix_time::time_duration td = tp.length();
  double total = (double)td.total_milliseconds() / 1000;

  cout << "Total time: " << total << " seconds" << endl;

  gtsam::Key c(1);
  Point3 initial(537.130, -769.991, -197.721); //FC21A0010040_11285085226F1T.LBL
  Vector3 vel((Vector(3) << .0163552, -.0241282, .1301240)); //FC21A0010040_11285085226F1T.LBL
  Point3 final(540.745, -775.335, -166.458); //FC21A0010041_11285085626F1T.LBL;

  SharedNoiseModel model(noiseModel::Isotropic::Sigma(6, 1.0));

  ast::UnaryGravityFactor f(vel, initial, final, model, c, 3, total, dt_);

  Vector actual = f.predict(coefficients);
  std::cout << "Error! \n " << actual << std::endl;
  Vector expected((Vector(3) << 0, 0, 0));
  EXPECT(assert_equal(expected, actual,1e-2));


  gtsam::Vector3 a_p = f.gravityAcceleration(coefficients, initial);
  gtsam::Vector3 a = -(mu*initial.vector()/pow(initial.norm(),3)) + a_p;
  std::cout << "Accel! \n " << a.norm() << std::endl;
}

/* ************************************************************************* */
int main() { TestResult tr; return TestRegistry::runAllTests(tr);}
/* ************************************************************************* */

