/*
 * @file testGravityFactor
 * @author Andrew Melim
 */

#include <CppUnitLite/TestHarness.h>
#include "GravityFactor.h"
#include <gtsam/inference/Key.h>

#include <boost/math/special_functions/legendre.hpp>
#include <boost/math/special_functions/factorials.hpp>
#include <boost/lexical_cast.hpp>

using namespace gtsam;
using namespace std;
using namespace ast;

static double fov = 60;
static size_t w=640,h=480;
static Cal3_S2::shared_ptr K(new Cal3_S2(fov,w,h));
static double dt_ = 0.5;
static size_t n_ = 3;
static Vector coefficients = (Vector(18) <<
  0.0, 0.0, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, // s cofs
  0.0, 0.0, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001);// c cofs
static Vector3 vel_((Vector(3) << 90000.0, 0.0, 120000.0));

///* ************************************************************************* */
//LieVector predictionError(const Pose3& p1, const Pose3& p2, const LieVector& cof,
//  const ast::GravityFactor& factor)
//{
//	return LieVector::Expmap(factor.evaluateError(cof, p1, p2));
//}
//
///* ************************************************************************* */
//TEST(GravityFactor, constructor){
//
//  gtsam::Key p1(1);
//  gtsam::Key p2(2);
//  gtsam::Key c(3);
//
//  Pose3 initial(Rot3(), Point3(0,500000, 0));
//  Pose3 expected(Rot3(), Point3(35229.37266483353, 500783.2777, 60000.000));
//  
//  SharedNoiseModel model(noiseModel::Isotropic::Sigma(6, 1.0));
//
//  ast::GravityFactor f(vel_, model, c, p1, p2, n_, dt_);
//  cout << f.numCoefs() << endl;
//  EXPECT(9 == f.numCoefs());
//}
//
///* ************************************************************************* */
//TEST(GravityFactor, leg)
//{
//  //Compared results to MATLAB call:
//  //P=legendre(i,sind(lat),'norm');
//  double sLat = sin(0);
//  int n = 1;
//  int m = 0;
//
//  //Fully normalized associated Laegendre functions
//  //http://mitgcm.org/~mlosch/geoidcookbook/node11.html
//  double actual = ast::GravityFactor::normedLegendre(n,m,sLat);
//  double expected = 0;
//
//	EXPECT_DOUBLES_EQUAL(expected, actual, 1e-8);
//
//  m = 1;
//  actual = ast::GravityFactor::normedLegendre(n,m,sLat);
//  expected = .866025403784438;
//
//	EXPECT_DOUBLES_EQUAL(expected, actual, 1e-8);
//}
//
///* ************************************************************************* */
//TEST(GravityFactor, calculatePotential)
//{
//  /****Set Variables****/
//  //gtsam::Vector s_cof = gtsam::sub(coefficients, 0, n_);
//  //gtsam::Vector c_cof = gtsam::sub(coefficients, n_, coefficients.size());
//  double s_cof = 0.0;
//  double c_cof = 0.0;
//  gtsam::Point3 point(537130, -769991, -197721);
//  //LBLData latLong(point);
//  //     
//  //double lat_ = latLong.latitude();
//  //double lon_ = latLong.longitude();
//  //double r = latLong.altitude();
//  //     
//  ////Convert to radians
//  //double sLat = sin(lat_*M_PI/180);
//  ////Reference Radius Konopliv 12
//  //double R = 265000;
////
//  ///***********/
//  //int n = 1;
//  //int m = 0;
////
//  //double expected = 0;
//  //double actual = GravityFactor::calculatePotential(n, m, lon_, lat_, r, sLat, R, s_cof, c_cof);
//	//EXPECT_DOUBLES_EQUAL(expected, actual, 1e-8);
////
//  //n = 2;
//  //m = 0;
//  //expected = -5.2629e-05;
//  //actual = GravityFactor::calculatePotential(n, m, lon_, lat_, r, sLat, R, 0.001, 0.001);
//	//EXPECT_DOUBLES_EQUAL(expected, actual, 1e-8);
//}
//
///* ************************************************************************* */
//TEST(GravityFactor, estimateGravity)
//{
//  Pose3 pose(Rot3(), Point3(537130, -769991, -197721));
//
//  //Compare expected gravitational component from MATLAB 
//  double expected =  3.6478e-05;//g at Pos 537130, -769991, -197721
//  double actual = GravityFactor::estimateGravity(coefficients, pose, 9);
//
//	EXPECT_DOUBLES_EQUAL(expected, actual, 1e-8);
//}
//
///* ************************************************************************* */
//TEST(GravityFactor, predict){
//
//	gtsam::Key p1(1);
//	gtsam::Key p2(2);
//	gtsam::Key c(3);
//
//  Pose3 initial(Rot3(), Point3(537130, -769991, -197721)); //FC21A0010040_11285085226F1T.LBL
//  Vector3 vel((Vector(3) << 16.3552, -24.1282, 130.1240)); //FC21A0010040_11285085226F1T.LBL
//  Pose3 expected(Rot3(), Point3(540745, -775335, -166458)); //FC21A0010041_11285085626F1T.LBL
//
//  Point3 v = expected.translation() - initial.translation();
//  v = v / 239.477;
//  Vector vActual = Point3::Logmap(v);
//
//  //EXPECT(assert_equal(vel, vActual, 1e-3));
//  
//	SharedNoiseModel model(noiseModel::Isotropic::Sigma(6, 1.0));
//
//  gtsam::LieVector coefs((Vector(18) <<
//    0.0, 0.0,         0.001, -1.13e-9, 4.247e-3, 0.001, 1.682e-3, -1.2177e-3, 1.5466e-4, // s cofs
//    0.0, 0.0, -3.1779397e-2 , 1.23e-9, 1.014e-3, 3.3106e-3, 2.045e-3, 6.5144e-4, 2.385e-3));
//
//	ast::GravityFactor f(vel, model, c, p1, p2, n_, 239.477);
//  //EXPECT(9 == f.numCoefs());
//	//Horrible arguments. actual isn't used for calculation
//	//Returns pose prediction in vector form
//  Vector predict(f.predict(coefs, initial, gtsam::Pose3(), 9, vel, 239.477 ));
//  Pose3 actual(gtsam::Pose3::Expmap(predict));
//
//  actual.print("Actual");
//
//	//EXPECT(expected.equals(actual));
//}

/* ************************************************************************* */
int main() { TestResult tr; return TestRegistry::runAllTests(tr);}
/* ************************************************************************* */
