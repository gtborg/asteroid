 /*
 * @file testKaulaFactor
 * @author Andrew Melim
 */

#include <CppUnitLite/TestHarness.h>
#include "KaulaFactor.h"
#include "utils.h"

#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

using namespace gtsam;

int n= 3;
TEST(KaulaFactor, constructor){

  Key c(1);
  SharedNoiseModel model(noiseModel::Isotropic::Sigma(6, 1.0));

  ast::KaulaFactor f(c, model, n);
}

TEST(KaulaFactor, error){
  gtsam::LieVector coefficients((Vector(12) << -1.13e-9, 4.2469730e-3,
                                              1.6820966e-3, -1.2177599e-3, 1.5466248e-4,
                                              -3.1779397e-2 , 1.23e-9, 1.0139517e-3,
                                              -3.3105530e-3, 2.0456938e-3, 6.5144047e-4, 2.3849359e-3));

  int dim_ = 3;
  gtsam::Vector error(dim_-1);

  size_t s_cof_Index=0;
  size_t c_cof_Index=2;

  // HARDCODED
  if(dim_ == 3){
    s_cof_Index=0;
    c_cof_Index=5;
  }

  for(size_t n=2; n < dim_+1; n++){

    double K = 0.011; // Vesta constant
    double expected = K/pow(n,2);
    double sum = 0;

    for(size_t m = 0; m < n; m++){

      double c_cof = coefficients[c_cof_Index];
      double s_cof = 0.0;
      c_cof_Index++;
      if(m > 0){
        s_cof = coefficients[s_cof_Index];
        s_cof_Index++;
      }

      sum += pow(c_cof,2) + pow(s_cof,2);
      std::cout << sum << std::endl;
    }

    double Mn = sqrt(sum/(2*n + 1));
    std::cout << "Mn: " << Mn << std::endl;
    std::cout << "K: " << expected << std::endl;
    error(n-2) = Mn - expected;

  }

  Vector expected2((Vector(3) << 0, 0, 0));
  // Note that the true solution for Vesta is off from the power law for the first few coefficients
  // This is due to the elliptical shape of the asteroid dominating the first few coefficients
  EXPECT(assert_equal(expected2, error, 1e-4));
}


/* ************************************************************************* */
int main() { TestResult tr; return TestRegistry::runAllTests(tr);}
/* ************************************************************************* */
