#include "utils.h"

#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/assign/std/set.hpp> // For += operator
#include <boost/range/algorithm/remove_if.hpp>
using namespace boost::assign;

#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;
using namespace ast;
using namespace monoslam;
using namespace gtsam;

/**
 * Load orbit from LBL file sub_sc_lat/long/alt
 * @param orbitDir Directly of the orbit to load
 */
vector<LBLData> loadLLAOrbit(const string &orbitDir){
  // Set of lines from which we need to read
  set<int> lines;
  lines += 71,197,198,204,205,206,209,210,211,246,159,201,247,249,252,250;

  vector<LBLData> llpVec;
// verify the path itself
  if (!boost::filesystem::is_directory(orbitDir)) {
    cerr << "Not a directory: " << orbitDir << endl;
    abort();
  }

  // load files into a vector for sorting
  vector<string> filenames;
  for (boost::filesystem::directory_iterator itr(orbitDir); itr
    != boost::filesystem::directory_iterator(); ++itr) {

    string fname = itr->path().filename().string();
    // check whether file is an LBL image
    if(fname.find(".LBL") != string::npos)
      filenames.push_back(fname);
  }

  sort(filenames.begin(), filenames.end());
  // Parse the files
  BOOST_FOREACH(string fname, filenames){

    LBLData lbl;
    lbl.setFilename(fname);
    int lineCount = 1;
    string fullp = orbitDir + "/" + fname;
    ifstream data(fullp.c_str());
    string line;

    while(getline(data,line)){
      if(lines.find(lineCount) != lines.end()){
        std::vector<std::string> strs;
        boost::split(strs, line, boost::is_any_of("=,<"));
        boost::erase_all(strs[1], " ");

        if(lineCount == 71){
          boost::replace_all(strs[1], "T", " ");
          //strs[1].substr to remove escape character
          boost::posix_time::ptime p(boost::posix_time::time_from_string(strs[1].substr(0,23)));
          lbl.setEndTime(p);
        }

        if(lineCount == 197) // Latitude
          lbl.setLatitude((boost::lexical_cast<double>(strs[1]) *M_PI) / 180);

        if(lineCount == 198) // Longitude
          lbl.setLongitude((boost::lexical_cast<double>(strs[1]) *M_PI) / 180);

        if(lineCount == 201) // Range to center of mass
          lbl.setTargetDistance(boost::lexical_cast<double>(strs[1]));

        if(lineCount == 204){ // Position x
          boost::erase_all(strs[0], " ");
          lbl.setX(boost::lexical_cast<double>(strs[0]));
        }
        if(lineCount == 205) // Position y
          lbl.setY(boost::lexical_cast<double>(strs[1]));

        if(lineCount == 206) // Position z
          lbl.setZ(boost::lexical_cast<double>(strs[1]));

        if(lineCount == 209){ // Velocity x
          boost::erase_all(strs[0], " ");
          lbl.setVX(boost::lexical_cast<double>(strs[0]));
        }
        if(lineCount == 210) // Velocity y
          lbl.setVY(boost::lexical_cast<double>(strs[1]));

        if(lineCount == 211) // Velocity z
          lbl.setVZ(boost::lexical_cast<double>(strs[1]));

        if(lineCount == 246) // Range to surface
          lbl.setAltitude(boost::lexical_cast<double>(strs[1]));

        if(lineCount == 247) // Top of camera frame
          lbl.setMinLatitude((boost::lexical_cast<double>(strs[1]) *M_PI) / 180);

        if(lineCount == 249) // Bottom of camera frame
          lbl.setMaxLatitude((boost::lexical_cast<double>(strs[1]) *M_PI) / 180);

        if(lineCount == 250) // Left edge of camera frame
          lbl.setWestLongitude((boost::lexical_cast<double>(strs[1]) *M_PI) / 180);

        if(lineCount == 252) // Right edge of camera frame
          lbl.setEastLongitude((boost::lexical_cast<double>(strs[1]) *M_PI) / 180);

        if(lineCount == 283) // Rotation of camera frame relative to the meridian
          lbl.setNorthAzimuth(boost::lexical_cast<double>(strs[1]));
      }
      lineCount++;
    }
    llpVec.push_back(lbl);
  }

  return llpVec;
}

/**
 * Initializes camera positions from latlongalt measurements. Backprojects points
 * @param llpVec     LatLongAlt vector
 */
Values initLLAOrbit(std::vector<ast::LBLData> llpVec,  const DSFEdgeMap& edgeMap, set<int> cams, Cal3_S2::shared_ptr K){

  typedef pair<Edge, set<Edge> > key_pair;

  Values initialEstimate;
  BOOST_FOREACH(int poseID, cams){
    Point3 lbl = llpVec[poseID].position_m();
    SimpleCamera camera = SimpleCamera::Lookat(lbl, Point3(), Point3(0.,0.,1.), *K);
    double naRads  = ((llpVec[poseID].northAzimuth()+90)*M_PI) / 180;
    Pose3 adjustNA = Pose3(Rot3::Rz(naRads), Point3()); // Rotate by North Azimuth
    camera = camera.compose(adjustNA);
    initialEstimate.insert(Symbol('x', poseID), camera.pose());
  }

  int nrLandmarks = 0;

  BOOST_FOREACH(key_pair kp, edgeMap.sets()){
    Edge init = kp.first;
    Point3 lInit;

    int poseKey = init.poseID;
    double backDist = llpVec[poseKey].altitude()*1000.;

    Pose3 posel = initialEstimate.at<Pose3>(Symbol('x', poseKey));
    gtsam::Point2 measl = init.measurement;// Get the first measurement

    SimpleCamera origin_camera(posel, *K);
    lInit = origin_camera.backproject(measl, backDist);
    initialEstimate.insert(Symbol('l', nrLandmarks), lInit);
    nrLandmarks++;
  }
  cout << "Initialized " << nrLandmarks << " landmarks." << endl;

  return initialEstimate;
}

/**
 * Checks for intersection of two DAWN camera images
 * @param  cam1 LBL data for first camera
 * @param  cam2 LBL data for second camera
 * @return      True if intersect, otherwise false
 */
bool intersect(const LBLData& cam1, const LBLData& cam2){
  return((cam1.westLongitude() < cam2.eastLongitude()) && (cam1.eastLongitude() > cam2.westLongitude()) &&
      (cam1.minLatitude() < cam2.maxLatitude()) && (cam1.maxLatitude() > cam2.minLatitude()));
}

/**
 * Returns the total time between two poses in seconds. Assumes l1 < l2
 * @param  l1 LBL data for first camera
 * @param  l2 LBL data for second camera
 * @return    Time in seconds (up to millisecond precision)
 */
double totalTimeBetween(const ast::LBLData& l1, const ast::LBLData& l2){
  boost::posix_time::time_period tp(l1.endTime(), l2.endTime());
  boost::posix_time::time_duration td = tp.length();
  double total = (double)td.total_milliseconds() / 1000;
  return total;
}
