/*
 * @file FITSData.h
 * @author Andrew Melim
 */

#pragma once

//GTSAM
#include <gtsam/geometry/Point3.h>

#include <math.h>
#include <iostream>
#define _USE_MATH_DEFINES

namespace ast {

class FITSData
{
    double lat_;
    double long_;
    double alt_; // backproject distance
    double na_; // North Azimuth
    double dist_; 

  public:

    FITSData() : lat_(0), long_(0), alt_(0), na_(0), dist_(0) {
    }

    FITSData(gtsam::Point3& point)
    {
      lat_ = atan(point.z()/sqrt(pow(point.x(),2) + pow(point.y(), 2)))*180/M_PI;
      long_ = atan2(point.y(), point.x())*180/M_PI;
      alt_ = sqrt(pow(point.x(), 2) + pow(point.y(), 2) + pow(point.z(),2));
    }

    FITSData(double x, double y, double z)
    {
      lat_ = atan(z/sqrt(pow(x,2) + pow(y, 2)))*180/M_PI;
      long_ = atan2(y, x)*180/M_PI;
      alt_ = sqrt(pow(x, 2) + pow(y, 2) + pow(z,2));
    }

    /*Get Methods*/
    double latitude() const { return lat_; }
    double longitude() const { return long_; }
    double altitude() const { return alt_; }
    double northAzimuth() const { return na_; }
    
    gtsam::Point3 position() const{
      double x = dist_*1000. * cos(lat_) * cos(long_);
      double y = dist_*1000. * cos(lat_) * sin(long_);
      double z = dist_*1000. * sin(lat_);
      return gtsam::Point3(x,y,z);
    }

    /*Set Methods*/
    void setLatitude(const double lat) { lat_ = lat; }
    void setLongitude(const double lon) { long_ = lon; }
    void setAltitude(const double alt) { alt_ = alt; }
    void setNorthAzimuth(const double na){ na_ = na;}
    void setTargetDistance(const double dist){ dist_ = dist;}
    void setLLA(const double lat, const double lon, const double alt) {
      lat_ = lat;
      long_ = lon;
      alt_ = alt;
    }
};

}
