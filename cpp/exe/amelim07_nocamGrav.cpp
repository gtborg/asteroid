#include <monoslam/BatchMono.h>
#include <monoslam/MatchEdge.h>
#include <monoslam/utils.h>

#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include "GravityFactor.h"

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>

#include "utils.h"

using namespace std;
using namespace gtsam;
using namespace monoslam;
using namespace ast;


int main(){

  BatchMono batchmono("../../config/", "config_gravity.cfg");
  Configuration config = batchmono.getConfig();

  string orbitdir = config.camInfoDirectory();
  string savePath = config.saveDirectory();
  string graphfile = savePath + "/vesta.graph";
  string iefile = savePath + "/initial.values";

  // Try to load saved GTSAM objects
  NonlinearFactorGraph graph = *loadGTSAM<NonlinearFactorGraph>(graphfile);

  Values initialEstimate = *loadGTSAM<Values>(iefile);
  vector<LBLData> llpVec = loadLLAOrbit(orbitdir);
  
  gtsam::LieVector coefs((Vector(18) <<
    0.0, 0.0, 0.001, -0.000000001, 0.004, 0.001, 0.001, -0.001, 0.0001, // s cofs
    0.0, 0.0, -0.01, 0.000000001, 0.001, 0.003, 0.001, 0.0001, 0.001));

  initialEstimate.insert(Symbol('c',1), coefs);

  // Up to degree 3
  //SharedNoiseModel model(noiseModel::Diagonal::Sigmas((gtsam::Vector(6) << Vector3::Constant(100.0), Vector3::Constant(1000.0)));
  noiseModel::Diagonal::shared_ptr poseNoise(
      noiseModel::Diagonal::Sigmas((gtsam::Vector(6) << Vector3::Constant(10),
       Vector3::Constant(10000.0))));

  SharedNoiseModel model(poseNoise);

  // Add our coefficient variables
  for(size_t i=0; i < llpVec.size(); i++){
    Point3 v = llpVec[i+1].position() - llpVec[i].position();
    boost::posix_time::time_duration td_dt = llpVec[i+1].endTime() - llpVec[i].endTime();
    double dt = td_dt.total_milliseconds()/1000.0;

    if(dt < 1000){
      v = v / dt;
    }else{
      continue; // Skip this since they are more than likely not dynamically linked
    }

    if(initialEstimate.exists(Symbol('x',i)) && initialEstimate.exists(Symbol('x',i+1)) ){
      Vector3 vel = Point3::Logmap(v);
      GravityFactor f(vel, model, Symbol('c',1), Symbol('x',i), Symbol('x',i+1), 3, dt);
      //graph.push_back(f);
    }
  }

  //LevenbergMarquardtParams parameters;
  //parameters.verbosity = NonlinearOptimizerParams::ERROR;
  //parameters.verbosityLM = LevenbergMarquardtParams::LAMBDA;
  //parameters.setMaxIterations(2000);
  //Values result = LevenbergMarquardtOptimizer(graph, initialEstimate, parameters).optimize();
  DoglegParams parameters;
  parameters.verbosity = NonlinearOptimizerParams::ERROR;
  //parameters.setMaxIterations(2000);
  Values result = DoglegOptimizer(graph, initialEstimate, parameters).optimize();
  //result.print("Final");
  logOutput(savePath+"/result/", result, graph);
  logMeshLab(savePath+"/result/", result, graph);

  cout << "Finished!" << endl;
  string resultfile = savePath + "/result.values";
  writeGTSAM<Values>(resultfile,  boost::make_shared<Values>(result));

}
