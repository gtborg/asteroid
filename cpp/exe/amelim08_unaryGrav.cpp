#include <monoslam/BatchMono.h>
#include <stdlib.h>
#include <time.h>

#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include "UnaryGravityFactor.h"
#include "KaulaFactor.h"

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>

#include "utils.h"

using namespace std;
using namespace gtsam;
using namespace monoslam;
using namespace ast;


int main(){

  BatchMono batchmono("../../config/", "config_gravity.cfg");
  Configuration config = batchmono.getConfig();

  string orbitdir = config.camInfoDirectory();

  Values initialEstimate;
  NonlinearFactorGraph graph;

  //gtsam::LieVector coefs((Vector(12) << -1.13e-9, 4.2469730e-3,
  //                                              1.6820966e-3, -1.2177599e-3, 1.5466248e-4,
  //                                              -3.1779397e-2 , 1.23e-9, 1.0139517e-3,
  //                                              -3.3105530e-3, 2.0456938e-3, 6.5144047e-4, 2.3849359e-3));

  gtsam::LieVector coefs((Vector(12) << 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1));

  size_t dim = 3;
  vector<LBLData> llpVec = loadLLAOrbit(orbitdir);

  initialEstimate.insert(Symbol('c',1), coefs);

  noiseModel::Diagonal::shared_ptr poseNoise(noiseModel::Diagonal::Sigmas((gtsam::Vector(3) << Vector3::Constant(0.0005))));//km

  noiseModel::Diagonal::shared_ptr kaulaNoise(noiseModel::Diagonal::Sigmas((gtsam::Vector(2) << 0.01, 0.001)));//non-dimensional

  SharedNoiseModel poseModel(poseNoise);
  SharedNoiseModel kaulaModel(kaulaNoise);

  cout << llpVec.size() << endl;

  string savePath = config.saveDirectory();
  string resultfile = savePath + "/result.values";
  boost::shared_ptr<Values> optimizedValues = loadGTSAM<Values>(resultfile);
  if(!optimizedValues){
    cout << "Could not open the result file: " << resultfile << endl;
    return 0;
  }
  //optimizedValues->print();


  ofstream log;
  log.open("log.txt");

  srand(time(NULL));
  int measurementCount = 0;

  int midx = 0;
  while(measurementCount < 20){

    double total = totalTimeBetween(llpVec[midx], llpVec[midx+1]);

    if(total >= 1000){
      midx += 1;
      continue;
    }


    //Point3 init(llpVec[midx].position());
    //Point3 final(llpVec[midx+1].position());
    Point3 init = optimizedValues->at<Pose3>(Symbol('x',midx)).translation()/1000.;
    Point3 final = optimizedValues->at<Pose3>(Symbol('x',midx+1)).translation()/1000.;
    //Vector3 vel = llpVec[midx].velocity();
    Vector3 vel = (final.vector() - init.vector())/total;

    init.print("init\n");
    final.print("expected\n");

    log << total << "\n";
    log << init.vector().transpose() << "\n";
    log << vel.transpose() << "\n";
    log << final.vector().transpose() << "\n";

    UnaryGravityFactor f(vel, init, final, poseModel, Symbol('c',1), dim, total, 0.01);
    graph.push_back(f);
    measurementCount++;

    midx += 1; // Take every 20th pose
  }

  // Kaula power law constraint
  KaulaFactor kaula(Symbol('c',1), kaulaModel, dim);
  graph.push_back(kaula);

  log.close();
  initialEstimate.print("Initial Values\n");
  //graph.print();

  LevenbergMarquardtParams parameters;
  parameters.verbosity = NonlinearOptimizerParams::ERROR;
  parameters.verbosityLM = LevenbergMarquardtParams::LAMBDA;
  parameters.setMaxIterations(2000);
  Values result = LevenbergMarquardtOptimizer(graph, initialEstimate, parameters).optimize();
  //DoglegParams parameters;
  //parameters.verbosity = NonlinearOptimizerParams::DELTA;
  ////parameters.setMaxIterations(2000);
  //Values result = DoglegOptimizer(graph, initialEstimate, parameters).optimize();
  result.print();
}
