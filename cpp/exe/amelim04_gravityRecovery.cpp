#include <MonoSLAM/HyperSFM/HyperSFM.h>
#include <MonoSLAM/HyperSFM/MatchEdge.h>
#include <MonoSLAM/utils.h>
#include <MonoSLAM/HyperSFM/HyperPlot.h>
#include <MonoSLAM/HyperSFM/HyperUtils.h>

#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include "utils.h"

using namespace std;
using namespace gtsam;
using namespace MonoSLAM;
using namespace cv;
using namespace ast;
int main(){
//  HyperSFM::Parameters params;
//  // Vesta
//  params.inputdir = "../../../../data/DWNVFC2_1B/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01/orig";
//  params.fileExt = ".png";
//  Cal3_S2::shared_ptr K(new Cal3_S2(10716.634266,10716.5813,0.0,511.5,511.5));
//  params.K = K; 
//  params.pointNoise = noiseModel::Diagonal::Sigmas((gtsam::Vec(3) << 500., 500., 500.));
//  params.poseNoise = noiseModel::Diagonal::Sigmas((gtsam::Vec(6) << Vector3::Constant(0.1), Vector3::Constant(0.1)));
//  params.correspondenceThresh = 8;
//
//
//  string orbitdir = "../../../../data/DWNVFC2_1B/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01";
//  string savePath = "../../../../monoTemp/nov_11_o1";
//  HyperSFM hypersfm(0);
//  hypersfm.setParameters(params);
//  vector<string> filenames = loadFileNames(params.inputdir, params.fileExt);
//  vector<LBLData> llpVec = loadLLAOrbit(orbitdir);
//
//  HyperGraph hypergraph = *(HyperGraph::loadGraph("cycle1_preTruss", savePath));
///*****************************************************
//   * Load pose info and initialize values, factor graph
//   *****************************************************/
//  cout << "Initializing factor graph" << endl;
//
//  // Connected components of the measurement edges in the factor graph
//  DSFEdgeMap edgemap = hypersfm.getEdgeMap();
//  Values initialEstimate = initLLAOrbit(llpVec, hypergraph, edgemap, params.K);
//
//  NonlinearFactorGraph graph = hypersfm.getFactorGraph();
//
//  /*****************************************************
//   * Add Landmark Priors
//   *****************************************************/
//  //size_t nrLmks = hypersfm.getLandmarkNum();
//  //cout << "nrLmks " << nrLmks << endl;
//  //for(size_t i = 0; i < nrLmks; i++){
//  //  Point3 priorL = initialEstimate.at<Point3>(Symbol('l',i));
//  //  graph.push_back(PriorFactor<Point3>(Symbol('l',i), priorL, params.pointNoise));
//  //}
//
//  /*****************************************************
//   * Add Pose Priors
//   *****************************************************/
//  for(unsigned int i = 0; i < filenames.size(); i++){
//    if(initialEstimate.exists(Symbol('x', i))){
//      Pose3 priorX = initialEstimate.at<Pose3>(Symbol('x',i));
//      graph.push_back(PriorFactor<Pose3>(Symbol('x',i), priorX, params.poseNoise));
//    }
//  }
//
//  cout << "Log" << endl;
//  logOutput(savePath+"/", initialEstimate, graph);
//  logMeshLab(savePath+"/", initialEstimate, graph);
//
//  /*****************************************************
//   * Optimize!
//   *****************************************************/
//
//  DoglegParams parameters;
//  parameters.verbosity = NonlinearOptimizerParams::ERROR;
//  //parameters.setMaxIterations(2000);
//  Values result = DoglegOptimizer(graph, initialEstimate, parameters).optimize();
//  //result.print("Final");
//  logOutput(savePath+"/result/", result, graph);
//  logMeshLab(savePath+"/result/", result, graph);
//
//  cout << "Finished!" << endl;
  return 1;
}