#include <monoslam/BatchMono.h>
#include <monoslam/MatchEdge.h>
#include <monoslam/utils.h>

#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include "utils.h"

using namespace std;
using namespace gtsam;
using namespace monoslam;
using namespace cv;
using namespace ast;
int main(){
  /*BatchMono::Parameters params;
  // Vesta
  params.inputdir = "../../../../data/DWNVFC2_1B/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01/images";
  params.fileExt = ".png";
  Cal3_S2::shared_ptr K(new Cal3_S2(10716.634266,10716.5813,0.0,511.5,511.5));
  params.K = K; 
  params.pointNoise = noiseModel::Diagonal::Sigmas((gtsam::Vector(3) << 500., 500., 500.));
  params.poseNoise = noiseModel::Diagonal::Sigmas((gtsam::Vector(6) << Vector3::Constant(0.1), Vector3::Constant(0.1)));
  params.correspondenceThresh = 8;


  string orbitdir = "../../../../data/DWNVFC2_1B/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01";
  string savePath = "../../../../monoTemp/oct_27_cycle1_o1";
  BatchMono batchmono(0);
  batchmono.setParameters(params);
  
  vector<LBLData> llpVec = loadLLAOrbit(orbitdir);

  // Connected components of the measurement edges in the factor graph
  DSFEdgeMap edgemap = batchmono.getEdgeMap();
  Values initialEstimate = initLLAOrbit(llpVec, edgemap, params.K);
  NonlinearFactorGraph graph = batchmono.getFactorGraph(hypergraph, 5);

  //Point3 priorL = initialEstimate.at<Point3>(Symbol('l',0));
  size_t nrHE = hypergraph.getHyperEdges().size();
  for(size_t i = 0; i < nrHE; i++){
    cout << i << endl;
    Point3 priorL = initialEstimate.at<Point3>(Symbol('l',i));
    graph.push_back(PriorFactor<Point3>(Symbol('l',i), priorL, params.pointNoise));
  }

  //cout << "Average track length: " << trackLength/nrHE << endl;
  for(int i = 0; i < 370; i++){
    if(initialEstimate.exists(Symbol('x', i))){
      Pose3 priorX = initialEstimate.at<Pose3>(Symbol('x',i));
      graph.push_back(PriorFactor<Pose3>(Symbol('x',i), priorX, params.poseNoise));
    }
  }
  cout << "Log" << endl;
  logOutput(savePath+"/", initialEstimate, graph);
  LevenbergMarquardtParams parameters;
  parameters.verbosity = NonlinearOptimizerParams::ERROR;
  parameters.verbosityLM = LevenbergMarquardtParams::LAMBDA;
  //parameters.lambdaInitial = 100;
  //cout << parameters.getMaxIterations() << endl;
  parameters.setMaxIterations(2000);
  //Values result = LevenbergMarquardtOptimizer(graph, initialEstimate, parameters).optimize();
  Values result = DoglegOptimizer(graph, initialEstimate).optimize();
  result.print("Final");
  logOutput(savePath+"/result/", result, graph);*/
}