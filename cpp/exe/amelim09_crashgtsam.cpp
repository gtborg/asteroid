
#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/base/LieVector.h>

using namespace std;
using namespace gtsam;

int main(){

  LieVector origin((Vector(3) << 0,1,1));
  LieVector measurement1((Vector(3) << 0,1,2));
  LieVector measurement2((Vector(3) << 0,1,2));
  LieVector measurement3((Vector(3) << 0,1,2));
  LieVector measurement4((Vector(3) << 0,1,2));

  noiseModel::Diagonal::shared_ptr pointNoise(noiseModel::Diagonal::Sigmas((gtsam::Vector(3) << Vector3::Constant(0.001))));//km
  SharedNoiseModel pointModel(pointNoise);

  Values initialEstimate;
  NonlinearFactorGraph graph;

  PriorFactor<LieVector> f1(Symbol('x',1), measurement1, pointModel);
  PriorFactor<LieVector> f2(Symbol('x',1), measurement2, pointModel);
  PriorFactor<LieVector> f3(Symbol('x',1), measurement3, pointModel);
  PriorFactor<LieVector> f4(Symbol('x',1), measurement4, pointModel);

  initialEstimate.insert(Symbol('x',1), origin);
  graph.push_back(f1);
  graph.push_back(f2);
  graph.push_back(f3);
  graph.push_back(f4);

  LevenbergMarquardtParams parameters;
  parameters.verbosity = NonlinearOptimizerParams::ERROR;
  parameters.verbosityLM = LevenbergMarquardtParams::LAMBDA;
  parameters.setMaxIterations(2000);
  Values result = LevenbergMarquardtOptimizer(graph, initialEstimate, parameters).optimize();
  //DoglegParams parameters;
  //parameters.verbosity = NonlinearOptimizerParams::DELTA;
  ////parameters.setMaxIterations(2000);
  //Values result = DoglegOptimizer(graph, initialEstimate, parameters).optimize();
  //result.print();
}
