#include <monoslam/BatchMono.h>
#include <stdlib.h>
#include <time.h>

#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include "UnaryGravityFactor.h"
#include "UnaryVelGravityFactor.h"
#include "KaulaFactor.h"

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>

#include "utils.h"

using namespace std;
using namespace gtsam;
using namespace monoslam;
using namespace ast;


int main(){

  BatchMono batchmono("../../config/", "config_gravity.cfg");
  Configuration config = batchmono.getConfig();

  string orbitdir = config.camInfoDirectory();

  Values initialEstimate;
  NonlinearFactorGraph graph;

  gtsam::LieVector coefs((Vector(12) << -1.13e-9, 4.2469730e-3,
                                                1.6820966e-3, -1.2177599e-3, 1.5466248e-4,
                                                -3.1779397e-2 , 1.23e-9, 1.0139517e-3,
                                                3.3105530e-3, 2.0456938e-3, 6.5144047e-4, 2.3849359e-3));

  //gtsam::LieVector coefs((Vector(12) << 0, 0.1, 0.1, -0.1, 0.1, -0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1));

  size_t dim = 3;
  vector<LBLData> llpVec = loadLLAOrbit(orbitdir);

  initialEstimate.insert(Symbol('c',1), coefs);

  noiseModel::Diagonal::shared_ptr poseNoise(noiseModel::Diagonal::Sigmas((gtsam::Vector(3) << Vector3::Constant(0.00001))));//km
  noiseModel::Diagonal::shared_ptr kaulaNoise(noiseModel::Diagonal::Sigmas((gtsam::Vector(2) << 0.01, 0.001)));//non-dimensional
  noiseModel::Diagonal::shared_ptr cofNoise(noiseModel::Diagonal::Sigmas((gtsam::Vector(12) << 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)));

  SharedNoiseModel poseModel(poseNoise);
  SharedNoiseModel kaulaModel(kaulaNoise);
  SharedNoiseModel cofModel(cofNoise);

  cout << llpVec.size() << endl;

  string savePath = config.saveDirectory();
  string resultfile = savePath + "/result.values";
  boost::shared_ptr<Values> optimizedValues = loadGTSAM<Values>(resultfile);
  if(!optimizedValues){
    cout << "Could not open the result file: " << resultfile << endl;
    return 0;
  }

  vector< pair<int,int> > arcs;
  // Oribit 1
  arcs.push_back(make_pair(0,2));
  arcs.push_back(make_pair(10,12));
  arcs.push_back(make_pair(20,22));
  arcs.push_back(make_pair(30,32));
  arcs.push_back(make_pair(40,42));
  arcs.push_back(make_pair(49,51));
  // Oribt 2
  arcs.push_back(make_pair(60,62));
  arcs.push_back(make_pair(70,72));
  arcs.push_back(make_pair(80,82));
  arcs.push_back(make_pair(90,92));
  arcs.push_back(make_pair(100,102));
  arcs.push_back(make_pair(110,112));
  // Oribit 3
  arcs.push_back(make_pair(120,122));
  arcs.push_back(make_pair(130,132));
  arcs.push_back(make_pair(140,142));



  for(int i = 0; i < arcs.size(); i++){
    pair<int, int> indx = arcs[i];
    double vTotal = totalTimeBetween(llpVec[indx.first], llpVec[indx.first+1]);
    Point3 init = optimizedValues->at<Pose3>(Symbol('x',indx.first)).translation()/1000.; //9899
    Point3 vel_p = optimizedValues->at<Pose3>(Symbol('x',indx.first+1)).translation()/1000.; //9900
    LieVector vel((vel_p.vector() - init.vector())/vTotal);
    initialEstimate.insert(Symbol('v',i), vel);

    double total = totalTimeBetween(llpVec[indx.first], llpVec[indx.second]);
    Point3 final = optimizedValues->at<Pose3>(Symbol('x',indx.second)).translation()/1000.; //9949

    cout << "vel: " << vel << endl;
    cout << "total: " << total << endl;
    init.print("init\n");
    final.print("expected\n");

    UnaryVelGravityFactor f(init, final, poseModel, Symbol('c',1), Symbol('v',i), dim, total, 0.01);
    graph.push_back(f);

  }

  PriorFactor<LieVector> cp(Symbol('c',1), coefs, cofModel);
  graph.push_back(cp);

  // Kaula power law constraint
  KaulaFactor kaula(Symbol('c',1), kaulaModel, dim);
  graph.push_back(kaula);

  initialEstimate.print("Initial Values\n");
  //graph.print();

  LevenbergMarquardtParams parameters;
  parameters.verbosity = NonlinearOptimizerParams::ERROR;
  parameters.verbosityLM = LevenbergMarquardtParams::LAMBDA;
  parameters.setMaxIterations(2000);
  Values result = LevenbergMarquardtOptimizer(graph, initialEstimate, parameters).optimize();
  //DoglegParams parameters;
  //parameters.verbosity = NonlinearOptimizerParams::DELTA;
  ////parameters.setMaxIterations(2000);
  //Values result = DoglegOptimizer(graph, initialEstimate, parameters).optimize();
  result.print();
}
