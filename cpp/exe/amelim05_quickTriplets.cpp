#include <monoslam/BatchMono.h>
#include <monoslam/MatchEdge.h>
#include <monoslam/utils.h>

#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/inference/Key.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>

#include "utils.h"

using namespace std;
using namespace gtsam;
using namespace monoslam;
using namespace cv;
using namespace ast;

/* ************************************************************************* */
// Export all classes derived from Value
// Required for serialization

void optimize(const NonlinearFactorGraph& graph,
    const Values& initialEstimate, const string& savePath){
  //LevenbergMarquardtParams parameters;
  //parameters.verbosity = NonlinearOptimizerParams::ERROR;
  //parameters.verbosityLM = LevenbergMarquardtParams::LAMBDA;
  //parameters.setMaxIterations(2000);
  //Values result = LevenbergMarquardtOptimizer(graph, initialEstimate, parameters).optimize();
  DoglegParams parameters;
  parameters.verbosity = NonlinearOptimizerParams::ERROR;
  //parameters.setMaxIterations(2000);
  Values result = DoglegOptimizer(graph, initialEstimate, parameters).optimize();
  //result.print("Final");
  logOutput(savePath+"/result/", result, graph);
  logMeshLab(savePath+"/result/", result, graph, true);

  cout << "Finished!" << endl;
  string resultfile = savePath + "/result.values";
  writeGTSAM<Values>(resultfile,  boost::make_shared<Values>(result));
}


int main(){

  bool loadSave = false;
  BatchMono batchmono("../../config/", "config_gravity.cfg");
  Configuration config = batchmono.getConfig();

  string savePath = config.saveDirectory();
  string graphfile = savePath + "/vesta.graph";
  string iefile = savePath + "/initial.values";

  string orbitdir = config.camInfoDirectory();

  // Try to load saved GTSAM objects
  boost::shared_ptr<NonlinearFactorGraph> graphP
    = loadGTSAM<NonlinearFactorGraph>(graphfile);

  boost::shared_ptr<Values> ieP = loadGTSAM<Values>(iefile);

  // If we were able to load successfully
  if(graphP && ieP && loadSave){
    optimize(*graphP, *ieP, savePath);
  }else{

    /*****************************************************
     * Parameters for BatchMono, load images into memory
     *****************************************************/

    noiseModel::Diagonal::shared_ptr pointNoise(
      noiseModel::Isotropic::Sigma(3, 10000.0));
    noiseModel::Diagonal::shared_ptr poseNoise(
      noiseModel::Diagonal::Sigmas((gtsam::Vector(6) << Vector3::Constant(10),
       Vector3::Constant(1.0))));

    vector<string> filenames = loadFileNames(config.inputDirectory(), config.fileExt());

    size_t max = 1000;
    MatchGraph matchgraph(max);

    /*****************************************************
     * Initial feature detection, association, and ransac
     *****************************************************/
    vector<LBLData> llpVec = loadLLAOrbit(orbitdir);
    size_t frame = 0;
    BOOST_FOREACH(string fname, filenames){
      if(frame < max)
        batchmono.loadImage(config.inputDirectory() + "/" + fname, frame);

      if(frame > 0 && frame < max){
        for(size_t i=0; i < frame; i++){
          if(intersect(llpVec[i], llpVec[frame])){
            string id1_s = boost::lexical_cast<string>(i);
            string id2_s = boost::lexical_cast<string>(frame);
            // /path/to/Match_id1_id2.dat
            string filename = savePath + "/Match_" + id1_s + "_" + id2_s + ".dat";
            ifstream ifs(filename.c_str());

            if(!ifs){
              MatchEdge edge = batchmono.matchPairs(i, frame);
              matchgraph.insert(edge, i, frame);
              MatchEdge::saveEdge(boost::make_shared<MatchEdge>(edge), i, frame, savePath);
            }else{
              MatchEdge edge = *(MatchEdge::loadEdge(i, frame, savePath));
              matchgraph.insert(edge, i, frame);
              //cout << "Loading " << i << " : " << frame << endl;
            }

          }else{
            //cout << "No intersection : " << i << " and " << frame << endl;
          }

        }// end for
      }
      frame++;
    }// End

    /*****************************************************
     * Triplet verification and merging
     *****************************************************/
    cout << "Finding triplets" << endl;
    cout << "MatchGraph size" << matchgraph.size() << endl;
    batchmono.findTriplets(matchgraph);

    MatchGraph::shared_ptr mg = boost::make_shared<MatchGraph>(matchgraph);
    MatchGraph::save(mg, savePath);

    /*****************************************************
     * Load pose info and initialize values, factor graph
     *****************************************************/
    cout << "Initializing factor graph" << endl;

    // Connected components of the measurement edges in the factor graph
    DSFEdgeMap edgemap = batchmono.getEdgeMap();
    Cal3_S2::shared_ptr K = batchmono.getCalibration();
    Values initialEstimate = initLLAOrbit(llpVec, edgemap, matchgraph.getCamIDs(), K);
    //Values initialEstimate = batchmono.getSeqInitialCameras(matchgraph);
    //initialEstimate = batchmono.backproject(initialEstimate, 1000);

    initialEstimate.print();

    NonlinearFactorGraph graph = batchmono.getFactorGraph();

    /*****************************************************
     * Add Landmark Priors
     *****************************************************/
    //size_t nrLmks = batchmono.getLandmarkNum();
    //cout << "nrLmks " << nrLmks << endl;
    //for(size_t i = 0; i < nrLmks; i++){
    //  Point3 priorL = initialEstimate.at<Point3>(Symbol('l',i));
    //  graph.push_back(PriorFactor<Point3>(Symbol('l',i), priorL, pointNoise));
    //}

    /*****************************************************
     * Add Pose Priors
     *****************************************************/
    for(size_t i = 0; i < max; i++){
      if(initialEstimate.exists(Symbol('x', i))){
        Pose3 priorX = initialEstimate.at<Pose3>(Symbol('x',i));
        graph.push_back(PriorFactor<Pose3>(Symbol('x',i), priorX, poseNoise));
      }
    }

    cout << "Log" << endl;
    logOutput(savePath+"/", initialEstimate, graph);
    logMeshLab(savePath+"/", initialEstimate, graph, true);

    // Save factor graph, initial estimate, and result

    writeGTSAM<NonlinearFactorGraph>(graphfile,
     boost::make_shared<NonlinearFactorGraph>(graph));

    writeGTSAM<Values>(iefile, boost::make_shared<Values>(initialEstimate));

    /*****************************************************
     * Optimize!
     *****************************************************/
    optimize(graph, initialEstimate, savePath);
  }
  return 1;

}
