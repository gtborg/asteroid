#include <monoslam/BatchMono.h>
#include <monoslam/MatchEdge.h>
#include <monoslam/utils.h>

using namespace std;
using namespace gtsam;
using namespace monoslam;
using namespace cv;

int main(int argc, char** argv){

  /*****************************************************
   * Parameters for BatchMono, load images into memory
   *****************************************************/

  noiseModel::Diagonal::shared_ptr pointNoise(
    noiseModel::Isotropic::Sigma(3, 10000.0));
  noiseModel::Diagonal::shared_ptr poseNoise(
    noiseModel::Diagonal::Sigmas((gtsam::Vector(6) << Vector3::Constant(10),
     Vector3::Constant(1.0))));


  BatchMono batchmono("../../config/", "config_cycle1_all.cfg");

  Configuration config = batchmono.getConfig();
  vector<string> filenames = loadFileNames(config.inputDirectory(), config.fileExt());
  //vector<string>::const_iterator first = filenames.begin();
  //vector<string>::const_iterator last = filenames.begin();
  //vector<string> subsetFiles(first, last);

  int frame = 0;
  BOOST_FOREACH(string fname, filenames){
    batchmono.loadImage(config.inputDirectory() + "/" + fname, frame);
    if(frame > 0){
      for(int i = 0; i< frame; i++){
        MatchEdge edge = batchmono.matchPairs(i,frame);
      }
    }
    ++frame;
  }
}