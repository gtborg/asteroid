#include "GravityFactor.h"


using namespace gtsam;

namespace ast {
  
  GravityFactor::GravityFactor(Vector3& vel, SharedNoiseModel& model,
    Key cofKey, Key pose1Key, Key pose2Key, size_t n, double dt) 
    : Base(model, cofKey, pose1Key, pose2Key), vel_(vel), n_(n), dt_(dt){
      nCof_ = 0;
      /**
       * nCof_ counts the number of coffecients for both c and s
       * Coefficient indecies are combination of n=1->n_ m=0->n
       * nCof_ for dim 1 = 2 (10 11)
       * nCof_ for dim 2 = 5 (10 11 20 21 22)
       * nCof_ for dim 3 = 9 (10 11 20 21 22 30 31 32 33)
       */
      for(size_t i = 1; i <= n; i++){
        nCof_ += i+1;
      }
  }

  void GravityFactor::print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter) const {
    std::cout << s << "Gravity factor" << std::endl;
  }
}
