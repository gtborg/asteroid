/**
 * @file GravityFactor
 * @brief Factor for estimating spherical harmonics of a gravitational field
 * @author Andrew Melim
 */

#pragma once

//BOOST
#include <boost/lexical_cast.hpp>
#include <boost/math/special_functions/legendre.hpp>
#include <boost/math/special_functions/factorials.hpp>
//GTSAM
#include <gtsam/geometry/concepts.h>
#include <gtsam/base/Vector.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/geometry/SimpleCamera.h>
#include <gtsam/nonlinear/NonlinearFactor.h>
//AST
#include "LBLData.h"

#include <math.h>

#define _USE_MATH_DEFINES

namespace ast {

  class GravityFactor : public gtsam::NoiseModelFactor3<gtsam::LieVector, gtsam::Pose3, gtsam::Pose3>
  {
    protected:

      gtsam::Vector3 vel_;
      //Degree of our coeffs
      size_t n_;
      size_t nCof_;
      double dt_;

    public:
      typedef gtsam::NoiseModelFactor3<gtsam::LieVector, gtsam::Pose3, gtsam::Pose3> Base;
      typedef GravityFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

      //Default Constructor
      GravityFactor() {
      }

      GravityFactor(gtsam::Vector3& vel, gtsam::SharedNoiseModel& model,
        gtsam::Key cofKey, gtsam::Key pose1Key,
        gtsam::Key pose2Key, size_t n, double dt);


      ~GravityFactor() {}

      //Make deep copy
      gtsam::NonlinearFactor::shared_ptr clone() const{
        return boost::static_pointer_cast<gtsam::NonlinearFactor>(
            gtsam::NonlinearFactor::shared_ptr(new GravityFactor(*this)));
      }

      void print(const std::string& s, const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      size_t numCoefs() const{
        return nCof_;
      }

      bool equals(const gtsam::NonlinearFactor& p, double tol=1e-9) const {
        const This* e = dynamic_cast<const This*>(&p);
        return e != NULL
          && Base::equals(p, tol)
          && (dt_ - e->dt_) < tol
          && (vel_ - e->vel_).norm() < tol;
          //&& this->vel_.equals(e->vel_,tol);
      }

      gtsam::Vector evaluateError(const gtsam::LieVector& cof,
          const gtsam::Pose3& pose1,
          const gtsam::Pose3& pose2,
          boost::optional<gtsam::Matrix&> H1 = boost::none,
          boost::optional<gtsam::Matrix&> H2 = boost::none,
          boost::optional<gtsam::Matrix&> H3 = boost::none) const{


        if (H1) {
          *H1 = gtsam::numericalDerivative31<gtsam::LieVector, gtsam::LieVector, gtsam::Pose3, gtsam::Pose3>(
          boost::bind(This::predict, _1, _2, _3, nCof_, vel_, dt_), cof, pose1, pose2, 1e-5);
        }

        if (H2) {
          *H2 = gtsam::numericalDerivative32<gtsam::LieVector, gtsam::LieVector, gtsam::Pose3, gtsam::Pose3>(
          boost::bind(This::predict, _1, _2, _3, nCof_, vel_, dt_), cof, pose1, pose2, 1e-5);
        }

        if (H3) {
          *H3 = gtsam::numericalDerivative33<gtsam::LieVector, gtsam::LieVector, gtsam::Pose3, gtsam::Pose3>(
          boost::bind(This::predict, _1, _2, _3, nCof_, vel_, dt_), cof, pose1, pose2, 1e-5);
        }

        gtsam::Vector prediction(predict(cof, pose1, pose2, nCof_, vel_, dt_));
        gtsam::Pose3 errPose(pose2.between(gtsam::Pose3::Expmap(prediction)));
        return gtsam::Pose3::Logmap(errPose);
      }

      /**
       * Computes the normalized associated legendre polynomial.
       * See mathematica notebook in docs for more information
       * @param  n Degree of the polynomial
       * @param  m Order of the polynomial
       * @param  x Value the polynomial is evaluated at
       * @return   Polynomial value
       */
      static double normedLegendre(int n, int m, double x) {
        double fact_num = boost::math::factorial<double>(n - m);
        double fact_denom = boost::math::factorial<double>(n + m);
        return boost::math::legendre_p(n,m,x) * pow(-1.0,m)*sqrt((((double)n + (1.0/2.0))*fact_num)/fact_denom);
      }

      static double calculatePotential(int n, int m, double lon_, double lat_, double r, double x, double R,
          double s_cof, double c_cof) {

        double pHat = normedLegendre(n,m,x);
        return pow(R/r, n)*pHat*(c_cof*cos((m*lon_)*M_PI/180) + s_cof*sin((m*lon_)*M_PI/180));
      }

      //Returns pose prediction from pose in vector form
      static double estimateGravity(const gtsam::LieVector& coefficients, const gtsam::Pose3& pose, size_t nCof) {
        if(coefficients.size() != (int)nCof*2){
          printf("ERROR: Mismatch in coefficients dimensions\n nCof: %i cs: %i", (int)nCof*2, (int)coefficients.size());
          throw;
        }
        gtsam::Vector s_cofV = gtsam::sub(coefficients, 0, nCof); // 0 -> nCof-1
        gtsam::Vector c_cofV = gtsam::sub(coefficients, nCof, coefficients.size()); // nCof -> 2*nCof
        //Convert Cart to LatLon
        gtsam::Point3 t(pose.translation());
        LBLData lbl; //TODO FIX

        double lat_ = lbl.latitude();
        double lon_ = lbl.longitude();
        double r = lbl.altitude();

        //Convert to radians
        double sLat = sin(lat_*M_PI/180);
        //int s_dim = (-3+sqrt(9+8*dim))/2 + 1;
        //Reference Radius Konopliv 12
        double R = 265000;
        // Gravitational constant
        double mu = 17.5;
        //u starts at 1 !!
        double u = 1;

        size_t cof_Index=0;
        //N 1 -> inf
        for(int n = 1; n < 3; n++)
        {

          //Compute i legendre polynomials
          for(int m = 0; m <= n; m++)
          {
            double s_cof = 0.0;
            double c_cof = 0.0;
            //std::cout << "n: " << n << " m: " << m << " k: " << k << std::endl;
            if(n > 1){ // Assume degree 1 (n==1) coefficients are zero
              s_cof = s_cofV[cof_Index];
              c_cof = c_cofV[cof_Index];
            }

            double pot = calculatePotential(n, m, lon_, lat_, r, sLat, R, s_cof, c_cof);
            u += pot;
            cof_Index++;
          }
        }

        double U = (mu/r) + u*mu/r;
        return U;
      }

      static gtsam::LieVector predict(const gtsam::LieVector& cof, const gtsam::Pose3& pose,
        const gtsam::Pose3& pose2, int nCof, gtsam::Vector vel_, double dt)
      {
        double t = 0;
        gtsam::Pose3 interP(pose);

        while(t < dt){
          double u = estimateGravity(cof, interP, nCof);
          vel_ = vel_ - u*0.01*interP.translation().vector()/interP.translation().norm();
          interP = interP.compose(gtsam::Pose3(gtsam::Rot3(), gtsam::Point3(vel_*0.01)));
          t += 0.01;
        }
        //gtsam::Pose3 predict(pose.compose(gtsam::Pose3(gtsam::Rot3(), gtsam::Point3(vel_*dt))));

        return gtsam::Pose3::Logmap(interP);
      }
  };//End Class
} //End Namespace
