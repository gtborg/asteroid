/*
 * @file asteroid.h
 * @author Andrew Melim
 */

virtual class gtsam::Value;
virtual class gtsam::NonlinearFactor;
class gtsam::KeySet;
class gtsam::KeyVector;
class gtsam::KeyList;
class gtsam::Pose3;
class gtsam::Point2;
class gtsam::Point3;
class gtsam::Cal3_S2;
class gtsam::noiseModel::Base;
class gtsam::noiseModel::Gaussian;
class gtsam::noiseModel::Diagonal;
class gtsam::noiseModel::Isotropic;


namespace ast{
  #include <Asteroid/GravityFactor.h>
  virtual class GravityFactor : gtsam::NonlinearFactor{
    GravityFactor();
    GravityFactor(Vector vel, gtsam::noiseModel::Base* model, 
        size_t k1, size_t k2, size_t k3, int n, double dt);

    //std ops
    void print(string s) const;

  };
}
