function [dX] = fullVesta(t,X)
%fullVesta includes a model for the Vesta gravity field through third order
%from 
%  Konopliv, A.S., et al. The Vesta gravity field, spin pole and rotation period,
%    landmark positions, and ephemeris from the Dawn tracking and optical data. 
%    Icarus (2013), http://dx.doi.org/10.1016/j.icarus.2013.09.005 
% Andrew Gisler

mu=17.288245;
Reff=265;

r=norm(X(1:3));

J2=3.1779397*10^-2;
c21=1.23e-9;
s21=-1.13e-9;
c22=1.0139517e-3;
s22=4.2469730e-3;
J3=-3.3105530e-3;
c31=2.0456938e-3;
s31=1.6820966e-3;
c32=6.5144047e-4;
s32=-1.2177599e-3;
c33=2.3849359e-3;
s33=1.5466248e-4;

c=[0,0,0,0;...
    0,0,0,0;...
    -J2,c21,c22,0;...
    -J3,c31,c32,c33];
s=[0,0,0,0;...
    0,0,0,0;...
    0,s21,s22,0;...
    0,s31,s32,s33];

dudr=0;
dudf=0;
dudl=0;
for ll=2:3
    p=legendre(ll,X(3)/r);
    p(length(p)+1)=0;
    for mm=0:ll
        dudr=dudr-mu/r^2*(Reff/r)^ll*(ll+1)*p(mm+1)*(c(ll+1,mm+1)*cos(mm*atan2(X(2),X(1)))+...
            s(ll+1,mm+1)*sin(mm*atan2(X(2),X(1))));
        dudf=dudf-mu/r*(Reff/r)^ll*(p(mm+2)-mm*X(3)/sqrt(X(1)^2+X(2)^2)*p(mm+1))*...
            (c(ll+1,mm+1)*cos(mm*atan2(X(2),X(1)))+s(ll+1,mm+1)*sin(mm*atan2(X(2),X(1)))); 
        dudl=dudl+mu/r*(Reff/r)^ll*mm*p(mm+1)*...
            (s(ll+1,mm+1)*cos(mm*atan2(X(2),X(1)))-c(ll+1,mm+1)*sin(mm*atan2(X(2),X(1))));
    end
end

        
ai = (1/r*dudr-X(3)/(r^2*sqrt(X(1)^2+X(2)^2))*dudf)*X(1)-...
    1/(X(1)^2+X(2)^2)*dudl*X(2);
aj = (1/r*dudr-X(3)/(r^2*sqrt(X(1)^2+X(2)^2))*dudf)*X(2)+...
    1/(X(1)^2+X(2)^2)*dudl*X(1);
ak = 1/r*dudr*X(3)+sqrt(X(1)^2+X(2)^2)/r^2*dudf;


dX=[X(4);...
    X(5);...
    X(6);...
    -mu*X(1)/r^3 + ai;...
    -mu*X(2)/r^3 + aj;...
    -mu*X(3)/r^3 + ak];

end