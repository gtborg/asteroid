function [data,structure] = loadOrbit(show, tree, verbose)
%% Create hypergraph of triplet matches
if nargin<1,show=false;end
if nargin<2,tree=false;end
if nargin<3,verbose=false;end

import gtsam.*

%% Set parameters
data.imageFormat = '/home/andrew/data/Dawn/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01/images/C1_ORBIT01%02d.png';
data.m=24;
data.scale=1;

% GTSAM objects cannot be saved (yet)
measurementNoiseSigma = 1.0;
data.measurementNoise = gtsam.noiseModel.Isotropic.Sigma(2,measurementNoiseSigma);
% f = 150.074mm, k = 8.4 10^-6[mm^-2]
data.K = gtsam.Cal3_S2(10716.5813,10716.5813,0,511.5,511.5);

%% Create priors on cameras
data.priors = Values;
target=Point3;
upVector=Point3(0,0,1);
m=data.m;

%% TODO: Fix camera initialization
lblFormat = '/home/andrew/data/Dawn/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01/';
lblFiles = dir(fullfile('/home/andrew/data/Dawn/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01/*.LBL'));

% Lat,lon,azi,alt : lines 197-200
for i=1:m
  fid = fopen([lblFormat lblFiles(i).name]);
  C = textscan(fid, '%s','delimiter','\n');
  
  % Here we initialize the camera values using the provided Lat Lon Data
  % INIT IT SUSPECT!! USE FUNDAMENTAL MATRIX
  
  latLine =  C{1}(197);
  latLine = strsplit(char(latLine));
  lat = str2double(latLine(3));
  
  lonLine =  C{1}(198);
  lonLine = strsplit(char(lonLine));
  lon = str2double(lonLine(3));
  
  altLine =  C{1}(200);
  altLine = strsplit(char(altLine));
  alt = str2double(altLine(3)) * 1000; % Convert to meters from km
  
  x = alt * cosd(lat) * cosd(lon);
  y = alt * cosd(lat) * sind(lon);
  z = alt * sind(lat);
  eye = Point3(x,y,z);
  camera = SimpleCamera.Lookat(eye,target, upVector,data.K);
  data.camera{i} = camera;
  data.priors.insert(symbol('x',i), camera.pose);
end
data.priorNoise = noiseModel.Diagonal.Sigmas([10 10 10 100 100 100]');

%% Perform matching and find triplets if not done yet
if exist('Orbit1.mat')
  load 'Orbit1.mat';
else
  %% Match
  [structure.nrMatches,structure.matchGraph] = matchAllPairs(data, 1.8, show,'F',300,0.1);
  
  %% find and merge triplets
  structure.hyperGraph = findTriplets(structure.nrMatches,structure.matchGraph,true);
  [structure.J,structure.n] = mergeTriplets(structure.hyperGraph,m);
  fprintf('found %d points after merging triplets\n',structure.n);
  
  %% Find truss
  structure.truss = hyperTruss(structure.hyperGraph,tree,verbose);
  
  save('Orbit1.mat','structure');
end
if show
  figure(1);clf
  imagesc(structure.nrMatches)
  colormap(hot)
end
if verbose
  structure.truss
end