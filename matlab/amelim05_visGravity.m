%% Visualize Gravity Field
% close all;
% clc;
% clear all;
clear
% s21=-1.13e-9;
% s22=4.2469730e-3;
% 
% s31=1.6820966e-3;
% s32=-1.2177599e-3;
% s33=1.5466248e-4;
% 
% J2=3.1779397*10^-2;
% c21=1.23e-9;
% c22=1.0139517e-3;
% 
% J3=-3.3105530e-3;
% c31=2.0456938e-3;
% c32=6.5144047e-4;
% c33=2.3849359e-3;

% 0.000849501; 0.0062847; 0.00216764; -0.00131142; 0.000107797; 
% -0.0574096; 0.000107422; 0.00192871; -0.00482714; 0.00299385; 0.0006028; 0.00243107

% 0.000207609; 0.0102151; 0.00405602; -0.00121777; 0.000154692;
% -0.108606; -0.000154692; 0.00240848; 0.00794259; 0.00497394; 0.000651426; 0.00238494

s21=0.000207609;
s22=0.0102151;

s31= 0.00405602;
s32= -0.00121777;
s33= 0.000107797;

J2=0.108606;
c21=-0.000154692;
c22= 0.00240848;

J3=-0.00794259;
c31=0.00497394;
c32=0.000651426;
c33=0.00238494;


c=[0,0,0,0;...
    0,0,0,0;...
    0,c21,c22,0;...
    -J3,c31,c32,c33];
s=[0,0,0,0;...
    0,0,0,0;...
    0,s21,s22,0;...
    0,s31,s32,s33];
  
  
  % Orbit Radius in meters
lat=-90:2:90;
long=-180:2:180;
mu=17.288245;
P = [];
for i=1:length(long)
    for j=1:length(lat)
      x = 265 * cos(lat(j)*pi/180) * cos(long(i)*pi/180);
      y = 265 * cos(lat(j)*pi/180) * sin(long(i)*pi/180);
      z = 265 * sin(lat(j)*pi/180);
      p = [x,y,z];
      [ai, aj, ak]=vestaGravAc(p,c,s);
      ap = -(mu*p)/(265^3);
      g(j,i)= (norm(ap + [ai, aj, ak]) - norm(ap))*1e8;
    end
end

figure('Color',[1 1 1]);
[LONG LAT]=meshgrid(long,lat);
pcolor(LONG,LAT,g);
shading interp
xlabel('Longitude')
ylabel('Lattitude')
colorbar

n=300;

Reff=265;
[X Y Z]=sphere(n);
X=265*X;
Y=265*Y;
Z=265*Z;
% C = zeros(n*n);
for i=1:length(X(:))
  p = [X(i),Y(i),Z(i)];
  [ai, aj, ak]=vestaGravAc(p,c,s);
  C(i) = norm([ai, aj, ak]);
end
C=reshape(C,n+1,n+1);


figure('Color',[1 1 1])
% subplot(1,2,2)
% Plottin
surf(X,Y,Z,C);
shading interp
axis equal;
axis off;
hold on;
grid off;
g=myaa;
% plot3(177.567, -258.971, -887.146, 'ro');



