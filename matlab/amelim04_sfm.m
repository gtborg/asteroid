% amelim04_sfm
import gtsam.*
% Clear figures
clf;

%% Structure from Motion via gtsam with the vesta dataset
show = false;
tree = false;
verbose = true;
[data,structure] = loadOrbit(show, tree, verbose);

%% Initialze graph
m = data.m;
x=cell(1,m);
keypoints=cell(1,m);
for i=1:m
    x{i} = symbol('x',i); % symbol to use
    keypoints{i} = cachedSift(data.imageFormat,i,data.scale);
end

% we add loose range factors on the points
rangePrior = noiseModel.Isotropic.Sigma(1,1000);
priorNoise = noiseModel.Diagonal.Sigmas([10; 10; 10; 100; 100; 100]);

graph = NonlinearFactorGraph;

initialEstimate = Values;
% Insert initial camera estimates
initialEstimate.insert(data.priors);

nrHyperEdges = length(structure.truss);
nrPoints = 0;

% Add pose priors on the first camera of each track
% graph.push_back(PriorFactorPose3(x{1}, data.camera{1}.pose, priorNoise));
% graph.push_back(PriorFactorPose3(x{9}, data.camera{9}.pose, priorNoise));

for h=1:nrHyperEdges
  fprintf(1,'Adding hyper edge %d from truss\n',h);
  hyperEdge = structure.truss(h);
  n1 = nrPoints+1;
  % Add the number of points from new hyperEdge
  nrPoints = nrPoints + size(hyperEdge.triplets, 2);
  depth = 684000; % meters, approximate camera altitude
  
  for t=n1:nrPoints % Iterator over new triplets
    for v=1:3 % For each camera in the triplet
      i = hyperEdge.i(v); % Camera ID
      k = hyperEdge.triplets(v,t-n1+1); % Feature ID in camera V
      z = keypoints{i}(1:2,k);
      p = Point2(z(1),z(2));
      j=structure.J{i}(k);
      key = symbol('p',j);
      graph.push_back(GenericProjectionFactorCal3_S2(p, data.measurementNoise, symbol('x',i), key, data.K));
      if ~initialEstimate.exists(key)
        % create an initial estimate by back-projecting:
        P = data.camera{i}.backproject(p,depth);
        initialEstimate.insert(key, P.retract(randn(3,1)));
        graph.push_back(RangeFactorPosePoint3(x{i}, key, depth, rangePrior));
      end
    end
  end
end

%% Optimize using Levenberg-Marquardt optimization with an ordering from colamd
% Optimize using Levenberg-Marquardt optimization with an ordering from colamd
fprintf(1,'initial error = %g\n',graph.error(initialEstimate));
parameters = LevenbergMarquardtParams;
parameters.setlambdaInitial(1000);
optimizer = LevenbergMarquardtOptimizer(graph, initialEstimate, parameters);
errors=zeros(1,50);lambdas=zeros(1,50);
for t=1:150
  optimizer.iterate
  errors(t)=optimizer.error;
  lambdas(t)=optimizer.lambda;
end
%optimizer.optimize;
result = optimizer.values();
error = graph.error(result);
fprintf(1,'final error = %g\n',graph.error(result));

plotSFM(data, result, initialEstimate, error, lambdas);
