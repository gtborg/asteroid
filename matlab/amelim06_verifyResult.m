import gtsam.*
% close all

total = 720.012;
init = [632.247, -594.113, 364.578];
vel = [-0.178749, -0.239644, -0.125562];
final = [490.631, -757.572, 273.302];

% 
J2=3.1779397*10^-2;
c21=1.23e-9;
s21=-1.13e-9;
c22=1.0139517e-3;
s22=4.2469730e-3;
J3=-3.3105530e-3;
c31=2.0456938e-3;
s31=1.6820966e-3;
c32=6.5144047e-4;
s32=-1.2177599e-3;
c33=2.3849359e-3;
s33=1.5466248e-4;

% 0.000103246; 0.00625012; 0.00243927; -0.00131075; 8.43656e-05; 
% -0.0574096; 0.000109666; 0.00173185; -0.00480443; 0.00297662; 0.000721485; 0.00238495

% s21=0.197732;
% s22=0.27622;
% 
% s31=0.633393;
% s32=-1.45428;
% s33=-3.00762;
% 
% J2=0.00415657;
% c21=-0.31888;
% c22=-1.58875;
% 
% J3= 0.066415;
% c31=0.74832;
% c32= 0.177778;
% c33= 2.83979;


c=[0,0,0,0;...
    0,0,0,0;...
    J2,c21,c22,0;...
    J3,c31,c32,c33];
s=[0,0,0,0;...
    0,0,0,0;...
    0,s21,s22,0;...
    0,s31,s32,s33];
  
%% Euler  
mu=17.288245;
Reff=265;
pose = Point3(init');
t = 0;
dt = 0.01;
A = [];
Ap = [];
Vel = [];
P = [];
while t < total
  pose = pose.compose(Point3((vel*dt)'));
  [ai, aj, ak] = vestaGravAc(pose.vector,c,s);
  Ap = [Ap; [ai, aj, ak]];
  a = -mu*(pose.vector')/(norm(pose.vector)^3) + [ai, aj, ak];
  A = [A; a];
  vel = vel + a*dt;
  Vel = [Vel; vel];
  P = [P; pose.vector'];
  t = t + dt;
end

expected = Point3(final');
expected.between(pose)


cpp = dlmread('/home/borg/borg/asteroid/build/cpp/log_predict.txt');

figure;
subplot(3,2,1)
plot(A(:,1)*1000)
hold on
plot(cpp(:,4)*1000,'r')
title('ai')

subplot(3,2,3)
plot(A(:,2)*1000)
hold on
plot(cpp(:,5)*1000,'r')
title('aj')

subplot(3,2,5)
plot(A(:,3)*1000)
hold on
plot(cpp(:,6)*1000,'r')
title('ak')
%%~~~~~~~~~~~~~~~~~~~~%%
subplot(3,2,2)
plot(Ap(:,1)*1000)
hold on
plot(cpp(:,1)*1000,'r')
title('aip')

subplot(3,2,4)
plot(Ap(:,2)*1000)
hold on
plot(cpp(:,2)*1000,'r')
title('ajp')

subplot(3,2,6)
plot(Ap(:,3)*1000)
hold on
plot(cpp(:,3)*1000,'r')
title('akp')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

figure;
subplot(3,3,1)
plot(Vel(:,1)*1000)
hold on
plot(cpp(:,7)*1000,'r')
title('Vx')

subplot(3,3,4)
plot(Vel(:,2)*1000)
hold on
plot(cpp(:,8)*1000,'r')
title('Vy')

subplot(3,3,7)
plot(Vel(:,3)*1000)
hold on
plot(cpp(:,9)*1000,'r')
title('Vz')
%%~~~~~~~~~~~~~~~~~~~~%%

subplot(3,3,2)
plot(P(:,1))
hold on
plot(cpp(:,10),'r')
title('Px')

subplot(3,3,5)
plot(P(:,2))
hold on
plot(cpp(:,11),'r')
title('Py')

subplot(3,3,8)
plot(P(:,3))
hold on
plot(cpp(:,12),'r')
title('Pz')

%%~~~~~~~~~~~~~~~~~~~~%%

% subplot(3,3,3)
% plot(abs(P(:,1) - cpp(:,10)))
% % hold on
% % plot(cpp(:,10),'r')
% title('Px Error')
% 
% subplot(3,3,6)
% plot(abs(P(:,2) - cpp(:,11)))
% % hold on
% % plot(cpp(:,11),'r')
% title('Py Error')
% 
% subplot(3,3,9)
% plot(abs(P(:,3) - cpp(:,12)))
% % hold on
% % plot(cpp(:,12),'r')
% title('Pz Error')


