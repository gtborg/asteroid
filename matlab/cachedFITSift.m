function [f,d] = cachedFITSift(fitsImage ,i,scale,verbose)
% cachedSift: check for file and if does not exist, extract SIFT features
% Checks for both key, key.gz, and .mat files.
% The two former ones are assumed to be Lowe's keypoint format
% The latter one is assumed to contain f and d from vl_sift
if nargin<4,verbose=false;end

% matFile = fullfile(dir,['image',i, '.mat']);
% 
% if exist(matFile,'file')==2
% 
%     % if a .mat exists, assume they are vl_sift features
%     load(matFile);
%     if verbose, fprintf(1,'read %d features from %s\n',size(f,2),[base '.mat']);end
%     
% else
    % no cached features, extract using vl_sift
    if verbose, fprintf('Processing image %d\n',i); end
    image = im2single(fitsImage.image);
    if nargin>=3 && scale~=1
        image = imresize(image,scale);
    end
    % Already greyscale
    %gray = mean(single(image),3);
    [f,d] = vl_sift(image);
%     save(matFile,'f','d');
end