function g=vestaGravCart(x,y,z)
    [lat long r] = convertToLatLongPosition(x, y, z);
    g=vestaGrav(r,lat,long);
end