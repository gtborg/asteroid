function generateLandmarkPoints(R)
    % Generates a set of 3D points on the sphere
    [X Y Z]=sphere(100);
    
    % Scale to provided radius
    X=R*X;
    Y=R*Y;
    Z=R*Z;
    for i=1:size(X,1)
        for j = 1:size(X,2)
           X(i,j) = X(i,j) + rand*500*(-2 + randi(3)); 
           Y(i,j) = Y(i,j) + rand*500*(-2 + randi(3)); 
           Z(i,j) = Z(i,j) + rand*500*(-2 + randi(3)); 
        end
       
    end

    surf(X,Y,Z)
    axis equal
end