import gtsam.*;

sfmResult;
gravResult;

gravKeys  = KeyVector(gravResult.keys);
sfmKeys = KeyVector(sfmResult.keys);

gravPoint = [];
sfmPoint = [];

gravPose = [];
sfmPose = [];

% Plot points and covariance matrices
for i = 0:gravKeys.size-1
    key = gravKeys.at(i);
    p = gravResult.at(key);
    if isa(p, 'gtsam.Point3')
            gravPoint = [gravPoint gtsam.Point3.Logmap(p)];
    end
end

% Plot points and covariance matrices
for i = 0:sfmKeys.size-1
    key = sfmKeys.at(i);
    p = sfmResult.at(key);
    if isa(p, 'gtsam.Point3')
            sfmPoint = [sfmPoint gtsam.Point3.Logmap(p)];
    end
end

% Plot points and covariance matrices
for i = 0:gravKeys.size-1
    key = gravKeys.at(i);
    p = gravResult.at(key);
    if isa(p, 'gtsam.Pose3')
            gravPose = [gravPose gtsam.Pose3.Logmap(p)];
    end
end

% Plot points and covariance matrices
for i = 0:sfmKeys.size-1
    key = sfmKeys.at(i);
    p = sfmResult.at(key);
    if isa(p, 'gtsam.Pose3')
            sfmPose = [sfmPose gtsam.Pose3.Logmap(p)];
    end
end

figure; hold on;
subplot(3,1,1)
plot(gravPose(1,:),'r-')
title('X error between SFM and Grav Factor in meters')
subplot(3,1,2)
plot(gravPose(2,:),'g-')
title('Y error between SFM and Grav Factor in meters')
subplot(3,1,3)
plot(gravPose(3,:),'b-')
title('Z error between SFM and Grav Factor in meters')

figure; hold on;
subplot(3,1,1)
plot(gravPose(1,:)-sfmPose(1,:),'r-')
title('X error between SFM and Grav Factor in meters')
subplot(3,1,2)
plot(gravPose(2,:)-sfmPose(2,:),'g-')
title('Y error between SFM and Grav Factor in meters')
subplot(3,1,3)
plot(gravPose(3,:)-sfmPose(3,:),'b-')
title('Z error between SFM and Grav Factor in meters')