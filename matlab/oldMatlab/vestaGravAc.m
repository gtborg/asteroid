function [ai, aj, ak]=vestaGravAc(X,c,s)
  % Andrew Gisler
  % Andrew Melim

  mu=17.288245;
  Reff=265;

  r=norm(X(1:3));

  dudr=0;
  dudf=0;
  dudl=0;
  for ll=2:3
      p=legendre(ll,X(3)/r,'norm');
      p(length(p)+1)=0;
      for mm=0:ll
          dudr=dudr-mu/r^2*(Reff/r)^ll*(ll+1)*p(mm+1)*(c(ll+1,mm+1)*cos(mm*atan2(X(2),X(1)))+...
              s(ll+1,mm+1)*sin(mm*atan2(X(2),X(1))));
            
          dudf=dudf-mu/r*(Reff/r)^ll*(p(mm+2)-mm*X(3)/sqrt(X(1)^2+X(2)^2)*p(mm+1))*...
              (c(ll+1,mm+1)*cos(mm*atan2(X(2),X(1)))+s(ll+1,mm+1)*sin(mm*atan2(X(2),X(1)))); 
            
          dudl=dudl+mu/r*(Reff/r)^ll*mm*p(mm+1)*...
              (s(ll+1,mm+1)*cos(mm*atan2(X(2),X(1)))-c(ll+1,mm+1)*sin(mm*atan2(X(2),X(1))));
      end
  end


  ai = (1/r*dudr-X(3)/(r^2*sqrt(X(1)^2+X(2)^2))*dudf)*X(1)-...
      1/(X(1)^2+X(2)^2)*dudl*X(2);
  aj = (1/r*dudr-X(3)/(r^2*sqrt(X(1)^2+X(2)^2))*dudf)*X(2)+...
      1/(X(1)^2+X(2)^2)*dudl*X(1);
  ak = 1/r*dudr*X(3)+(sqrt(X(1)^2+X(2)^2)/r^2)*dudf;
  
%   acc = norm([ai aj ak]);
end