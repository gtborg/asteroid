%class GravityFactor, see Doxygen page for details
%at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
%
%-------Constructors-------
%GravityFactor()
%
%-------Methods-------
%print(string s) : returns void
%
classdef GravityFactor < gtsam.NonlinearFactor
  properties
    ptr_GravityFactor = 0
  end
  methods
    function obj = GravityFactor(varargin)
      if (nargin == 2 || (nargin == 3 && strcmp(varargin{3}, 'void'))) && isa(varargin{1}, 'uint64') && varargin{1} == uint64(5139824614673773682)
        if nargin == 2
          my_ptr = varargin{2};
        else
          my_ptr = Asteroid_wrapper(1, varargin{2});
        end
        base_ptr = Asteroid_wrapper(0, my_ptr);
      elseif nargin == 0
        [ my_ptr, base_ptr ] = Asteroid_wrapper(2);
      else
        error('Arguments do not match any overload of GravityFactor constructor');
      end
      obj = obj@gtsam.NonlinearFactor(uint64(5139824614673773682), base_ptr);
      obj.ptr_GravityFactor = my_ptr;
    end

    function delete(obj)
      Asteroid_wrapper(3, obj.ptr_GravityFactor);
    end

    function display(obj), obj.print(''); end
    %DISPLAY Calls print on the object
    function disp(obj), obj.display; end
    %DISP Calls print on the object
    function varargout = print(this, varargin)
      % PRINT usage: print(string s) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      % 
      % Method Overloads
      % print(string s)
      if length(varargin) == 1 && isa(varargin{1},'char')
        Asteroid_wrapper(4, this, varargin{:});
      else
        error('Arguments do not match any overload of function GravityFactor.print');
      end
    end

  end

  methods(Static = true)
  end
end
