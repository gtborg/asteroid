clc;

% Initial Conditions
% Velocity
v=[2000 0 0]; %m/s
% Position
p=[465000 0 0]; %meters
% Angular Rate
w=1.11871958; %deg/s
n=50;

import gtsam.*

dt=0.01;
figure;     
hold on;

pose_id = 0;
for i=1:1000
    g=vestaGravCart(p(i,1),p(i,2),p(i,3));
    % Velocity
    v(i+1,:)=v(i,:)-g*dt*p(i,:)/norm(p(i,:));
    p(i+1,:)=p(i,:)+v(i,:)*dt;
    p(i+1,:)=[cosd(w) -sind(w) 0; sind(w) cosd(w) 0; 0 0 1]*p(i+1,:)';
    

    pose_id = pose_id + 1;
end

[X Y Z]=sphere(n);
X=265000*X;
Y=265000*Y;
Z=265000*Z;
for i=1:length(X(:))
    C(i)=vestaGravCart(X(i),Y(i),Z(i));
end
C=reshape(C,n+1,n+1);


groundTruth = p;
% Plotting
surf(X,Y,Z,C);
plot3(p(:,1),p(:,2),p(:,3));
axis equal;





