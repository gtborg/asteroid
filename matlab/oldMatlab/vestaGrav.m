function g=vestaGrav(r,lat,long)
    % Mass
    M=258.9*10^18;
    % Reference Radius 265-km
    % Derived page 12 of Konopliv
    R=265;
    % Degree 1 coefficients equal 0 so center of gravity is at the origin
    % Vesta is approximately degree 15
%     C=[0 0 .0001 .001 .001 .0001 .0001 .0001 .0001 .0001 .00001 .001 .0001 .00001];
%     S=[0 0 0 .001 .001 0 .0001 .0001 .0001 0 .00001 .00001 .0001 .0001];
% -0.370685; -2.77765; 3.55737; 7.47463; 0.114434
    C = [0.0, 0.0, 0.0317799, 0, 0.00036];
    S = [0.0, 0.0, 0, 0, 0.00435];
    Cfts=[C;S];
    g=gravity(r,lat,long,M,R,Cfts);
end