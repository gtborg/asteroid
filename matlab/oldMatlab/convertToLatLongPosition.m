function [lat long r] = convertToLatLongPosition(x, y, z)
    r2 = x^2+y^2;
    lat = atan(z/sqrt(r2))*180/pi;
    long = atan2(y,x)*180/pi;
    r = sqrt(r2+z^2);
end