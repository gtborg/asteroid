clc;
clear all;

% Orbit Radius in meters
r=265000;
lat=-90:5:90;
long=-180:5:180;
P = [];
for i=1:length(long)
    for j=1:length(lat)
      x = 500 * cos(lat(j)) * cos(long(i));
      y = 500 * cos(lat(j)) * sin(long(i));
      z = 500 * sin(lat(j));
      
      p = [x,y,z];
      P = [P; p];
      g(j,i)=vestaGrav(r,lat(j),long(i));
    end
end
figure;
subplot(1,2,1)
[LONG LAT]=meshgrid(long,lat);
pcolor(LONG,LAT,g);
xlabel('Longitude')
ylabel('Lattitude')
colorbar

n=50;
[X Y Z]=sphere(n);
X=265000*X;
Y=265000*Y;
Z=265000*Z;
for i=1:length(X(:))
    C(i)=vestaGravCart(X(i),Y(i),Z(i));
end
C=reshape(C,n+1,n+1);

subplot(1,2,2)
% Plotting
% surf(X,Y,Z,C);
axis equal;

for i=1:length(P)
  plot3(P(i,1),P(i,2),P(i,3),'*');
  hold on;
end
