% amelim02_features

%% Reading
data_dir = '/home/andrew/data/Dawn/2011272_HAMO/2011272_CYCLE1/2011272_C1_ORBIT01/'
if(~exist('images', 'var'))
  images = readOrbit(data_dir);
end

%% Diplay
figure(1); clf; axis image off;
colormap(gray)
imagesc(cat(2, images{1}.image, images{2}.image))

% imshow(images{1}.image,[images{1}.low images{1}.high], 'Border', 'tight')
% 
% imshow(images{2}.image,[images{2}.low images{2}.high], 'Border', 'tight')

%% Computation
imgA = im2single(images{1}.image);
imgB = im2single(images{2}.image);
% frames = vl_covdet(img)

% A frame is a disk of center f(1:2), scale f(3) and orientation f(4)
[fa,da] = vl_sift(imgA);
[fb,db] = vl_sift(imgB);
[matches, scores] = vl_ubcmatch(da, db);

xa = fa(1,matches(1,:));
xb = fb(1,matches(2,:)) + size(imgA,2);
ya = fa(2,matches(1,:));
yb = fb(2,matches(2,:));

hold on ;
h = line([xa ; xb], [ya ; yb]);
set(h,'linewidth', 0.2, 'color', 'b');

vl_plotframe(fa(:,matches(1,:)));
fb(1,:) = fb(1,:) + size(imgA,2);
vl_plotframe(fb(:,matches(2,:)));
axis image off ;

