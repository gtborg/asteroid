%% Reading
write = true;
data_dir = '/home/borg/data/DAWN/2011272_HAMO/2011289_CYCLE4/2011293_C4_ORBIT10/';
%% imageFormat = '/home/borg/data/DWNVFC2_1B/2011272_HAMO/2011284_CYCLE3/images/C1_ORBIT01_%08d.png';
imageFormat = '/home/borg/data/DAWN/2011272_HAMO/CYCLE34/png/%s.png';
images = readOrbit(data_dir);

% writerObj = VideoWriter('orbit1.avi');
% open(writerObj);

for k=1:length(images)
%   imshow(images{k}.image,[images{k}.low images{k}.high], 'Border', 'tight')
%   frame = getframe;
%   writeVideo(writerObj,frame);
  if(write)
    s = strsplit(images{k}.data.Filename, '/');
    fn = strsplit(cell2mat(s(9)),'.');
    img_name = sprintf(imageFormat,fn{1})
    I = mat2gray(images{k}.image, [images{k}.low images{k}.high]);
    imwrite(I, img_name, 'png');
  end
end

% close(writerObj);

