function [nrMatches,matchGraph] = matchVestaPairs(data,threshold,show,check,maxRounds,ransacSigma)
% matchAllPairs: match all images against all others
% NOTE: This is modified due to the FITS data format
% [nrMatches,matchGraph] = matchVestaPairs(data,threshold,show,check,maxRounds,ransacSigma)
%   data:
%       imageFormat: string to read images
%       m: number of images to read
%       scale: between 0 and 1, factor to scale images with
%   threshold: parameter to ucb_match
%   show: show flow before and after consistency check, and pause
%   check: type geometric consistency check: 'none', 'H', or 'F'
%   softMatchMatrix: used for choosing which images to match
% returns:
%   m*m matrix with number of matches
%   m*m cell with the matches

if ~isstruct(data), error('new version requires data'); end
if nargin<3, show=false;end
if nargin<4, check='none';end
if nargin<5, maxRounds=100; end

% Read images and extract SIFT features
disp('Reading images');
m=10;%data.m;
sigma=zeros(1,m);
keypoints=cell(1,m);
descriptors=cell(1,m);
for i=1:m
    if nargin<6
        [width, height]=size(data.images{m}.image);
        sigma(i) = 0.001*max(width,height); % Snavely07ijcv 6% seemed too loose
    else
        sigma(i) = ransacSigma;
    end
    [keypoints{i},descriptors{i}] = cachedFITSift(data.images{m},i,data.scale,true);
end

% Perform matching
if nargin>6
    disp('Starting exhaustive matching');
else 
    disp('Starting soft matching');
end

nrMatches=zeros(m,m);
matchGraph=cell(m,m);
for i1=1:m-1
    for i2=i1+1:m
        if nargin>6 && ~softMatchMatrix(i1,i2), continue; end;
        [matchGraph{i1,i2},putatives] = matchPair(...
            keypoints{i1},descriptors{i1},keypoints{i2},descriptors{i2},...
            threshold,check,maxRounds,sigma(i1),sigma(i2));
        nrMatches(i1,i2)=size(matchGraph{i1,i2},2);
        if (show)
            im1 = data.images{i1}.image;
            im2 = data.images{i2}.image;
            showPair(im1,keypoints{i1},im2,keypoints{i2},data.scale,matchGraph{i1,i2},putatives);
            title(sprintf('Matches between %d and %d',i1, i2));
            pause;
        end
    end
end

end