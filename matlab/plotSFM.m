function plotSFM(data, result, initialEstimate, error, lambdas)

import gtsam.*

%% Show initialization
% figure();clf
% plotVisualSLAMValues(data.priors,[],10,5,18);
% title('Priors');

%% Plot Initial estimate
% figure();
% plotVisualSLAMValues(initialEstimate,[],10,5,18);
% title('Initial Estimate');

% figure; 
% subplot(2,1,1)
% grid on;
% semilogy(1:50,error,'r.');
% title('Error');
% subplot(2,1,2)
% grid on;
% semilogy(1:50,lambdas,'b.');
% title('Lambdas');


%% Plot Result
% figure; grid on;
% plotVisualSLAMValues(result,[],5,5,18)
% view(3)
% title('Result');


%% Plot satellite trajectory
poseKeys = utilities.allPose3s(result).keys;
ieKeys = utilities.allPose3s(initialEstimate).keys;
satResult = zeros(3,poseKeys.size);
satInit = zeros(3,poseKeys.size);
satZ = zeros(2,poseKeys.size);
for i=1:poseKeys.size
    key = poseKeys.front();
    poseKeys.pop_front();
    pose_i = result.at(key);
    satResult(1,i) = pose_i.translation.x;
    satResult(2,i) = pose_i.translation.y;
    satResult(3,i) = pose_i.translation.z;  
    
    iekey = ieKeys.front();
    ieKeys.pop_front();
    pose_i = initialEstimate.at(iekey);
    satInit(1,i) = pose_i.translation.x;
    satInit(2,i) = pose_i.translation.y;
    satInit(3,i) = pose_i.translation.z; 
end

figure;
plotVisualSLAMValues(result,[],10,5,18);
hold on;
plot3(satInit(1,:), satInit(2,:), satInit(3,:),'b');
plot3(satResult(1,:), satResult(2,:), satResult(3,:),'r');

title('Result vs. Initial est');
end