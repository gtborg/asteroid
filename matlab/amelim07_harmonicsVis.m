

close all
s21=0;
s22=0;

s31=0;
s32=0;
s33=0;

J2=0;
c21=0;
c22=0;

J3= 0;
c31=0;
c32=0;
c33=0;

c=[0,0,0,0;...
    0,0,0,0;...
    0,0,0,0;...
    0,0,0,1];
s=[0,0,0,0;...
    0,0,0,0;...
    0,0,0,0;...
    0,0,0,1];


n=100;
mu=17.288245;
Reff=265;
[X Y Z]=sphere(n);
X=265*X;
Y=265*Y;
Z=265*Z;
for i=1:length(X(:))
  p = [X(i),Y(i),Z(i)];
  [ai, aj, ak]=vestaGravAc(p,c,s);
  C(i) = norm([ai, aj, ak]);
%   C(i)=norm(-mu*p/(Reff^3) + C(i));
end
C=reshape(C,n+1,n+1);

figure('Color',[1 1 1])
% subplot(1,2,2)
% Plottin
surf(X,Y,Z,C);
shading interp
axis equal;
axis off;
hold on;
grid off;
g=myaa;
% plot3(177.567, -258.971, -887.146, 'ro');