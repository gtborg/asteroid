function images = readOrbit(data_dir)
% Andrew Melim
% Read orbit folder fit files
% Loads a single directory of FIT files, often a single orbit
% Provides lowest and highest pixel value for easy image scaling
% Also provides data from fitinfo

files = dir([data_dir '*.FIT']);

for i = 1:size(files,1)
  file_name = [data_dir files(i).name];
  try
    meas = fitsread(file_name);
    data = fitsinfo(file_name);
    high = max(max(meas));
    low = min(min(meas));
%     imshow(meas,[low high]);
%     k = waitforbuttonpress;
    
    images{i}.image = meas;
    images{i}.high = high;
    images{i}.low = low;
    images{i}.data = data;
    
  catch
    fprintf('File exception, probably empty file')
    file_name
  end
end

%%