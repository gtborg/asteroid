function [r_an,v_an,Xnum,a,ecc,incl,Om,om,nu,t] = vesta_orbit(x0,v0)
%Starting with an initial position x0 and initial velocity v0 (in km and
%km/s respectively) this finds position and velocity as well as the orbital
%elements for orbiting around Vesta. D
% Andrew Gisler

time=(0:1:239);
X0=[x0;v0];

mu=17.28867;

atol=1e-12;
rtol=1e-12;
options=odeset('RelTol',rtol,'AbsTol',[atol atol atol atol atol atol]);

% can use fullVesta for Vesta gravity field through 3rd order
% can use twobody for the two body equations of motion
% can use twobodyplusJ2 for the addition of the J2 term
[t,Xnum]=ode23(@fullVesta,time,X0,options);
a(1:length(t),1)=0;
ecc(1:length(t),1)=0;
incl(1:length(t),1)=0;
Om(1:length(t),1)=0;
om(1:length(t),1)=0;
nu(1:length(t),1)=0;
for ii=1:length(t)
    [~,aii,eccii,inclii,Omii,omii,nii] = rv2coe(Xnum(ii,1:3),Xnum(ii,4:6),mu);
    a(ii)=aii;ecc(ii)=eccii;incl(ii)=inclii;Om(ii)=Omii;om(ii)=omii;nu(ii)=nii;
end

[p0,a0,ecc0,incl0,Om0,om0,nu0] = rv2coe(x0,v0,mu);
% [r_an,v_an] = COE2RV_times(p0,a0,ecc0,incl0,Om0,om0,nu0,time);


end
