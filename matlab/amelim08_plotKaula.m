s21=-1.13e-9;
s22=4.2469730e-3;

s31=1.6820966e-3;
s32=-1.2177599e-3;
s33=1.5466248e-4;

J2=3.1779397*10^-2;
c21=1.23e-9;
c22=1.0139517e-3;

J3=-3.3105530e-3;
c31=2.0456938e-3;
c32=6.5144047e-4;
c33=2.3849359e-3;

c=[0,0,0,0;...
    0,0,0,0;...
    -J2,c21,c22,0;...
    -J3,c31,c32,c33];
s=[0,0,0,0;...
    0,0,0,0;...
    0,s21,s22,0;...
    0,s31,s32,s33];

approx=[];
kaula = [];
k = 0.011;
for x=2:10
  if(x < 4)
    sum = 0;
    for ll=2:3
        for mm=0:ll
          sum  = sum + (s(ll+1,mm+1)^2 + c(ll+1,mm+1)^2);
        end
    end
    approx = [approx sqrt(sum/(2*x + 1))];
  end
  kaula = [kaula k/(x^2)];
end

  
figure
plot(approx,'r')
hold on
plot(kaula,'b')
